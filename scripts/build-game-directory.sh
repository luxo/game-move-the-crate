#!/bin/sh

mkdir -p ../bin
mkdir -p ../bin/data
mkdir -p ../bin/data/levels

convert ../art-source/graphics.xcf -layers merge -type truecolor ../bin/data/graphics.bmp
convert ../art-source/font-16x16.xcf -layers merge -type truecolor ../bin/data/font-16x16.bmp

cp ../levels/* ../bin/data/levels
