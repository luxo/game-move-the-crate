#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#include "allegro5/allegro.h"

#include "logfile.h"
#include "struct_map.h"

enum
  {
  MAP_POSITION_DISABLED = -1
  };

enum
  {
  MAP_LEVEL_FILE_HEADER_SIZE = 4
  };

const char level_file_header[] = "PTCL";

const int blob_lookup_table[ 256 ] =
  {
   0,   1,   0,   1,   2,   3,   2,   4,
   0,   1,   0,   1,   2,   3,   2,   4,
   5,   6,   5,   6,   7,   8,   7,   9,
   5,   6,   5,   6,  10,  11,  10,  12,
   0,   1,   0,   1,   2,   3,   2,   4,
   0,   1,   0,   1,   2,   3,   2,   4,
   5,   6,   5,   6,   7,   8,   7,   9,
   5,   6,   5,   6,  10,  11,  10,  12,
  13,  14,  13,  14,  15,  16,  15,  17,
  13,  14,  13,  14,  15,  16,  15,  17,
  18,  19,  18,  19,  20,  21,  20,  22,
  18,  19,  18,  19,  23,  24,  23,  25,
  13,  14,  13,  14,  15,  16,  15,  17,
  13,  14,  13,  14,  15,  16,  15,  17,
  26,  27,  26,  27,  28,  29,  28,  30,
  26,  27,  26,  27,  31,  32,  31,  33,
   0,   1,   0,   1,   2,   3,   2,   4,
   0,   1,   0,   1,   2,   3,   2,   4,
   5,   6,   5,   6,   7,   8,   7,   9,
   5,   6,   5,   6,  10,  11,  10,  12,
   0,   1,   0,   1,   2,   3,   2,   4,
   0,   1,   0,   1,   2,   3,   2,   4,
   5,   6,   5,   6,   7,   8,   7,   9,
   5,   6,   5,   6,  10,  11,  10,  12,
  13,  34,  13,  34,  15,  35,  15,  36,
  13,  34,  13,  34,  15,  35,  15,  36,
  18,  37,  18,  37,  20,  38,  20,  39,
  18,  37,  18,  37,  23,  40,  23,  41,
  13,  34,  13,  34,  15,  35,  15,  36,
  13,  34,  13,  34,  15,  35,  15,  36,
  26,  42,  26,  42,  28,  43,  28,  44,
  26,  42,  26,  42,  31,  45,  31,  46
  };

enum
  {
  NEIGHBOUR_COUNT = 8
  };

struct s_neighbour
  {
  int x;
  int y;
  };

const struct s_neighbour neighbour[ NEIGHBOUR_COUNT ] =
  {
  {  0, -1 }, {  1, -1 }, {  1,  0 }, {  1,  1 },
  {  0,  1 }, { -1,  1 }, { -1,  0 }, { -1, -1 }
  };

static bool fl_is_inside_map          ( int x, int y );
static int  fl_calculate_blob         ( struct s_map *map, int x, int y );
static void fl_recalculate_blob_data  ( struct s_map *map );
static void fl_remove_empty_tiles     ( struct s_map *map );
static void fl_place_empty_tiles      ( struct s_map *map );
static void fl_remove_all_crates      ( struct s_map *map );
static void fl_remove_all_destinations( struct s_map *map );
static bool fl_look_for_crate         ( struct s_map *map, int x, int y );
static bool fl_look_for_destination   ( struct s_map *map, int x, int y );
static bool fl_look_for_start_position( struct s_map *map, int x, int y );

static bool fl_is_inside_map( int x, int y )
  {
  if( ( x >= 0 && x < MAP_WIDTH ) &&
      ( y >= 0 && y < MAP_HEIGHT ) )
    return true;

  return false;
  }

static int fl_calculate_blob( struct s_map *map, int x, int y )
  {
  int i;
  int tile;
  int index;

  assert( map );
  assert( ( x >= 0 && x < MAP_WIDTH ) && ( y >= 0 && y < MAP_HEIGHT ) );

  index = 0;

  tile = map->tile[ x ][ y ];

  for( i=0; i<NEIGHBOUR_COUNT; i++ )
    {
    int nx;
    int ny;
    int neighbour_tile;

    nx = x + neighbour[ i ].x;
    ny = y + neighbour[ i ].y;

    if( fl_is_inside_map( nx, ny ) )
      neighbour_tile = map->tile[ nx ][ ny ];
    else
      neighbour_tile = tile;

    if( tile == neighbour_tile )
      index += ( 1 << i );
    }

  return blob_lookup_table[ index ];
  }

static void fl_recalculate_blob_data( struct s_map *map )
  {
  int i, j;

  assert( map );

  for( i=0; i<MAP_WIDTH; i++ )
    for( j=0; j<MAP_HEIGHT; j++ )
        map->blob_id[ i ][ j ] = fl_calculate_blob( map, i, j );
  }

static void fl_remove_empty_tiles( struct s_map *map )
  {
  int i, j;

  assert( map );

  for( i=0; i<MAP_WIDTH; i++ )
    for( j=0; j<MAP_HEIGHT; j++ )
      if( map->tile[ i ][ j ] == MAP_TILE_EMPTY )
        map->tile[ i ][ j ] = MAP_TILE_WALL;
  }

static void fl_place_empty_tiles( struct s_map * map )
  {
  int i, j, k;
  int value;

  assert( map );

  for( i=0; i<MAP_WIDTH; i++ )
    for( j=0; j<MAP_HEIGHT; j++ )
      {
      value = 0;

      if( map->tile[ i ][ j ] == MAP_TILE_WALL )
        {
        for( k=0; k<NEIGHBOUR_COUNT; k++ )
          {
          int x, y;

          x = i + neighbour[ k ].x;
          y = j + neighbour[ k ].y;

          if( fl_is_inside_map( x, y ) )
            {
            if( map->tile[ x ][ y ] == MAP_TILE_WALL ||
                map->tile[ x ][ y ] == MAP_TILE_EMPTY )
              value += ( 1 << k );
            }
          else
            {
            value += ( 1 << k );
            }
          }

        if( value == 255 )
          {
          map->tile[ i ][ j ] = MAP_TILE_EMPTY;
          // Also remove decorations:
          map->decoration[ i ][ j ] = 0;
          }
        }
      }
  }

static void fl_remove_all_crates( struct s_map *map )
  {
  int i;

  assert( map );

  map->crate_count = 0;

  for( i=0; i<MAP_MAX_CRATES; i++ )
    {
    map->crate[ i ].x = MAP_POSITION_DISABLED;
    map->crate[ i ].y = MAP_POSITION_DISABLED;
    }
  }

static void fl_remove_all_destinations( struct s_map *map )
  {
  int i;

  assert( map );

  map->destination_count = 0;

  for( i=0; i<MAP_MAX_DESTINATIONS; i++ )
    {
    map->destination[ i ].x = MAP_POSITION_DISABLED;
    map->destination[ i ].y = MAP_POSITION_DISABLED;
    }
  }

//   Look for a crate on x,y coordinates.
//   Returns true if there is a crate on given coordinates.
static bool fl_look_for_crate( struct s_map *map, int x, int y )
  {
  int i;

  assert( map );

  for( i=0; i<map->crate_count; i++ )
    if( map->crate[ i ].x == x && map->crate[ i ].y == y )
      return true;

  return false;
  }

// Look for a destination on x,y coordinates.
// Returns true if there is a destination on given coordinates.
static bool fl_look_for_destination( struct s_map *map, int x, int y )
  {
  int i;

  assert( map );

  for( i=0; i<map->destination_count; i++ )
    if( map->destination[ i ].x == x && map->destination[ i ].y == y )
      return true;

  return false;
  }

// Look for a player start position on x,y coordinates.
// Returns true if there is a player start position on given coordinates.
static bool fl_look_for_start_position( struct s_map *map, int x, int y )
  {
  assert( map );

  if( map->start_x_pos == x && map->start_y_pos == y )
    return true;

  return false;
  }

struct s_map *map_create_map_struct ()
  {
  struct s_map *map;
  int i, j;

  map = NULL;
  map = malloc( sizeof( struct s_map ) );
  if( !map )
    {
    logfile_write( "MAP: Failed to create map struct." );
/*
    printf( "MAP: Failed to create map struct!\n" );
*/
    return NULL;
    }

  // Default values:
  for( i=0; i<MAP_WIDTH; i++ )
    for( j=0; j<MAP_HEIGHT; j++ )
      {
      map->tile[ i ][ j ] = 0;
      map->blob_id[ i ][ j ] = 0;
      map->decoration[ i ][ j ] = 0;
      }

  map->start_x_pos     = MAP_POSITION_DISABLED;
  map->start_y_pos     = MAP_POSITION_DISABLED;
  map->start_direction = 0;

  fl_remove_all_crates      ( map );
  fl_remove_all_destinations( map );

  return map;
  }

void map_destroy_map_struct( struct s_map *map )
  {
  assert( map );

  free( map );
  }

int map_get_tile_info( struct s_map *map, int x, int y )
  {
  int i;
  bool is_crate;
  bool can_walk;

  assert( map );

  if( ( x < 0 || x > MAP_WIDTH ) ||
      ( y < 0 || y > MAP_HEIGHT ) )
    return MAP_TILE_INFO_BLOCKED;

  is_crate = false;
  can_walk = false;

  if( map->tile[ x ][ y ] == MAP_TILE_FLOOR )
    can_walk = true;

  for( i=0; i<map->crate_count; i++ )
    if( map->crate[ i ].x == x && map->crate[ i ].y == y )
      is_crate = true;

  if( is_crate )
    return MAP_TILE_INFO_CRATE;

  if( can_walk )
    return MAP_TILE_INFO_CAN_WALK;

  return MAP_TILE_INFO_BLOCKED;
  }

int map_get_crate_id( struct s_map *map, int x, int y )
  {
  int i;

  assert( map );

  for( i=0; i<map->crate_count; i++ )
    if( map->crate[ i ].x == x && map->crate[ i ].y == y )
      return i;

  return -1; // No crate on given position.
  }

void map_move_crate( struct s_map *map, int crate_id, int x, int y )
  {
  assert( map );

  map->crate[ crate_id ].x += x;
  map->crate[ crate_id ].y += y;
  }

// Save map to file:
int map_save_map_to_file( struct s_map *map, const char *file_path )
  {
  int i;
  ALLEGRO_FILE *file;

  assert( map );

  file = al_fopen( file_path, "wb" );
  if( !file )
    return MAP_SAVE_FAILED_TO_OPEN_FILE;

  al_fwrite( file, level_file_header, MAP_LEVEL_FILE_HEADER_SIZE );

  al_fwrite( file, map->tile, MAP_WIDTH * MAP_HEIGHT );
  al_fwrite( file, map->blob_id, MAP_WIDTH * MAP_HEIGHT );
  al_fwrite( file, map->decoration, MAP_WIDTH * MAP_HEIGHT );

  al_fwrite( file, &map->crate_count, 1 );
  for( i=0; i<MAP_MAX_CRATES; i++ )
    {
    al_fwrite( file, &map->crate[ i ].x, 1 );
    al_fwrite( file, &map->crate[ i ].y, 1 );
    }

  al_fwrite( file, &map->destination_count, 1 );
  for( i=0; i<MAP_MAX_DESTINATIONS; i++ )
    {
    al_fwrite( file, &map->destination[ i ].x, 1 );
    al_fwrite( file, &map->destination[ i ].y, 1 );
    }

  al_fwrite( file, &map->start_x_pos    , 1 );
  al_fwrite( file, &map->start_y_pos    , 1 );
  al_fwrite( file, &map->start_direction, 1 );

  al_fclose( file );

  return MAP_SAVE_OK;
  }

// Load map from file
int map_load_map_from_file( struct s_map *map, const char *file_path )
  {
  int i;
  char file_header[ 8 ];
  ALLEGRO_FILE *file;

  assert( map );

  file = NULL;
  file = al_fopen( file_path, "rb" );
  if( !file )
    return MAP_LOAD_FAILED_TO_OPEN_FILE;

  al_fread( file, file_header, MAP_LEVEL_FILE_HEADER_SIZE );
  file_header[ MAP_LEVEL_FILE_HEADER_SIZE ] = 0;
  if( strcmp( file_header, level_file_header ) != 0 )
    {
    al_fclose( file );
    return MAP_LOAD_NOT_A_MAP_FILE;
    }

  al_fread( file, map->tile, MAP_WIDTH * MAP_HEIGHT );
  al_fread( file, map->blob_id, MAP_WIDTH * MAP_HEIGHT );
  al_fread( file, map->decoration, MAP_WIDTH * MAP_HEIGHT );

  al_fread( file, &map->crate_count, 1 );
  for( i=0; i<MAP_MAX_CRATES; i++ )
    {
    al_fread( file, &map->crate[ i ].x, 1 );
    al_fread( file, &map->crate[ i ].y, 1 );
    }

  al_fread( file, &map->destination_count, 1 );
  for( i=0; i<MAP_MAX_DESTINATIONS; i++ )
    {
    al_fread( file, &map->destination[ i ].x, 1 );
    al_fread( file, &map->destination[ i ].y, 1 );
    }

  al_fread( file, &map->start_x_pos    , 1 );
  al_fread( file, &map->start_y_pos    , 1 );
  al_fread( file, &map->start_direction, 1 );

  al_fclose( file );

  return MAP_LOAD_OK;
  }

// Make map empty:
void map_clear_map( struct s_map *map )
  {
  int i, j;

  assert( map );

  fl_remove_all_crates( map );
  fl_remove_all_destinations( map );

  for( i=0; i<MAP_WIDTH; i++ )
    for( j=0; j<MAP_HEIGHT; j++ )
      {
      map->tile[ i ][ j ] = MAP_TILE_WALL;
      map->decoration[ i ][ j ] = 0;
      }

  for( i=1; i<MAP_WIDTH-1; i++ )
    for( j=1; j<MAP_HEIGHT-1; j++ )
      map->tile[ i ][ j ] = MAP_TILE_FLOOR;

  fl_recalculate_blob_data( map );

  map->start_x_pos = MAP_POSITION_DISABLED;
  map->start_y_pos = MAP_POSITION_DISABLED;
  map->start_direction = 0;
  }

// Editor stuff:
void map_editor_place_wall( struct s_map *map, int x, int y )
  {
  assert( map );

  if( fl_look_for_crate( map, x, y ) )
    return;

  if( fl_look_for_destination( map, x, y ) )
    return;

  if( fl_look_for_start_position( map, x, y ) )
    return;

  fl_remove_empty_tiles( map );

  map->tile[ x ][ y ] = MAP_TILE_WALL;
  map->decoration[ x ][ y ] = 0;

  fl_place_empty_tiles    ( map );
  fl_recalculate_blob_data( map );
  }

void map_editor_place_floor( struct s_map *map, int x, int y )
  {
  assert( map );

  if( map->tile[ x ][ y ] != MAP_TILE_FLOOR )
    {
    fl_remove_empty_tiles( map );

    map->tile[ x ][ y ] = MAP_TILE_FLOOR;
    map->decoration[ x ][ y ] = 0;

    fl_place_empty_tiles    ( map );
    fl_recalculate_blob_data( map );
    }
  }

void map_editor_place_crate( struct s_map *map, int x, int y )
  {
  assert( map );

  if( map->crate_count >= MAP_MAX_CRATES )
    return;

  if( map->tile[ x ][ y ] == MAP_TILE_WALL )
    return;

  if( fl_look_for_crate( map, x, y ) )
    return;

  if( fl_look_for_destination( map, x, y ) )
    return;

  if( fl_look_for_start_position( map, x, y ) )
    return;

  map->crate[ map->crate_count ].x = x;
  map->crate[ map->crate_count ].y = y;
  map->crate_count++;
  }

void map_editor_place_destination( struct s_map *map, int x, int y )
  {
  assert( map );

  if( map->destination_count >= MAP_MAX_DESTINATIONS )
    return;

  if( map->tile[ x ][ y ] == MAP_TILE_WALL )
    return;

  if( fl_look_for_crate( map, x, y ) )
    return;

  if( fl_look_for_destination( map, x, y ) )
    return;

  if( fl_look_for_start_position( map, x, y ) )
    return;

  map->destination[ map->destination_count ].x = x;
  map->destination[ map->destination_count ].y = y;
  map->destination_count++;

  map->decoration[ x ][ y ] = 0;
  }

void map_editor_place_start_position( struct s_map *map, int x, int y )
  {
  assert( map );

  if( map->tile[ x ][ y ] == MAP_TILE_WALL )
    return;

  if( fl_look_for_crate( map, x, y ) )
    return;

  if( fl_look_for_destination( map, x, y ) )
    return;

  map->start_x_pos = x;
  map->start_y_pos = y;
  }

void map_editor_delete_crate( struct s_map *map, int x, int y )
  {
  int i, j;

  assert( map );

  for( i=0; i<map->crate_count; i++ )
    if( x == map->crate[ i ].x && y == map->crate[ i ].y )
      {
      for( j=i; j<map->crate_count-1; j++ )
        {
        map->crate[ j ].x = map->crate[ j + 1 ].x;
        map->crate[ j ].y = map->crate[ j + 1 ].y;
        }
      map->crate_count--;

      map->crate[ map->crate_count ].x = MAP_POSITION_DISABLED;
      map->crate[ map->crate_count ].y = MAP_POSITION_DISABLED;
      break;
      }
  }

void map_editor_delete_destination( struct s_map *map, int x, int y )
  {
  int i, j;

  assert( map );

  for( i=0; i<map->destination_count; i++ )
    if( x == map->destination[ i ].x && y == map->destination[ i ].y )
      {
      for( j=i; j<map->destination_count-1; j++ )
        {
        map->destination[ j ].x = map->destination[ j + 1 ].x;
        map->destination[ j ].y = map->destination[ j + 1 ].y;
        }
      map->destination_count--;

      map->destination[ map->destination_count ].x = MAP_POSITION_DISABLED;
      map->destination[ map->destination_count ].y = MAP_POSITION_DISABLED;
      break;
      }
  }

void map_editor_delete_start_position( struct s_map *map, int x, int y )
  {
  assert( map );

  if( x == map->start_x_pos && y == map->start_y_pos )
    {
    map->start_x_pos = MAP_POSITION_DISABLED;
    map->start_y_pos = MAP_POSITION_DISABLED;
    map->start_direction = 0;
    }
  }

void map_shift_up( struct s_map *map )
  {
  int i, j;
  int tile_backup[ MAP_WIDTH ];
  int deco_backup[ MAP_WIDTH ];

  assert( map );

  fl_remove_empty_tiles( map );

  // Backup the top most tiles:
  for( i=1; i<MAP_WIDTH-1; i++ )
    {
    tile_backup[ i ] = map->tile[ i ][ 1 ];
    deco_backup[ i ] = map->decoration[ i ][ 1 ];
    }

  // Shift map one tile up:
  for( j=1; j<MAP_HEIGHT-1; j++ )
    for( i=1; i<MAP_WIDTH-1; i++ )
      {
      map->tile[ i ][ j ] = map->tile[ i ][ j + 1 ];
      map->decoration[ i ][ j ] = map->decoration[ i ][ j + 1 ];
      }

  // Restore the backup at the bottom of the map:
  for( i=1; i<MAP_WIDTH-1; i++ )
    {
    map->tile[ i ][ MAP_HEIGHT - 2 ] = tile_backup[ i ];
    map->decoration[ i ][ MAP_HEIGHT - 2 ] = deco_backup[ i ];
    }

  // Shift crates:
  for( i=0; i<map->crate_count; i++ )
    {
    map->crate[ i ].y -= 1;

    if( map->crate[ i ].y < 1 )
      map->crate[ i ].y = MAP_HEIGHT - 2;
    }

  // Shift destinations:
  for( i=0; i<map->destination_count; i++ )
    {
    map->destination[ i ].y -= 1;

    if( map->destination[ i ].y < 1 )
      map->destination[ i ].y = MAP_HEIGHT -2;
    }

  // Shift player start position:
  if( map->start_y_pos != MAP_POSITION_DISABLED )
    {
    map->start_y_pos -= 1;
    if( map->start_y_pos < 1 )
      map->start_y_pos = MAP_HEIGHT - 2;
    }

  fl_place_empty_tiles    ( map );
  fl_recalculate_blob_data( map );
  }

void map_shift_right( struct s_map *map )
  {
  int i, j;
  int tile_backup[ MAP_HEIGHT ];
  int deco_backup[ MAP_HEIGHT ];

  assert( map );

  fl_remove_empty_tiles( map );

  for( j=1; j<MAP_HEIGHT-1; j++ )
    {
    tile_backup[ j ] = map->tile[ MAP_WIDTH - 2 ][ j ];
    deco_backup[ j ] = map->decoration[ MAP_WIDTH - 2 ][ j ];
    }

  for( i=MAP_WIDTH-2 ; i>1; i-- )
    for( j=1; j<MAP_HEIGHT-1; j++ )
      {
      map->tile[ i ][ j ] = map->tile[ i - 1 ][ j ];
      map->decoration[ i ][ j ] = map->decoration[ i - 1 ][ j ];
      }

  for( j=1; j<MAP_HEIGHT-1; j++ )
    {
    map->tile[ 1 ][ j ] = tile_backup[ j ];
    map->decoration[ 1 ][ j ] = deco_backup[ j ];
    }

  // Shift crates:
  for( i=0; i<map->crate_count; i++ )
    {
    map->crate[ i ].x += 1;

    if( map->crate[ i ].x > MAP_WIDTH - 2 )
      map->crate[ i ].x = 1;
    }

  // Shift destinations:
  for( i=0; i<map->destination_count; i++ )
    {
    map->destination[ i ].x += 1;

    if( map->destination[ i ].x > MAP_WIDTH - 2 )
      map->destination[ i ].x = 1;
    }

  // Shift player start position:
  if( map->start_x_pos != MAP_POSITION_DISABLED )
    {
    map->start_x_pos += 1;
    if( map->start_x_pos > MAP_WIDTH - 2 )
      map->start_x_pos = 1;
    }

  fl_place_empty_tiles    ( map );
  fl_recalculate_blob_data( map );
  }


void map_shift_down ( struct s_map *map )
  {
  int i, j;
  int tile_backup[ MAP_WIDTH ];
  int deco_backup[ MAP_WIDTH ];

  assert( map );

  fl_remove_empty_tiles( map );

  for( i=1; i<MAP_WIDTH-1; i++ )
    {
    tile_backup[ i ] = map->tile[ i ][ MAP_HEIGHT - 2 ];
    deco_backup[ i ] = map->decoration[ i ][ MAP_HEIGHT - 2 ];
    }

  for( j=MAP_HEIGHT-2; j>1; j-- )
    for( i=1; i<MAP_WIDTH-1; i++ )
      {
      map->tile[ i ][ j ] = map->tile[ i ][ j - 1 ];
      map->decoration[ i ][ j ] = map->decoration[ i ][ j - 1 ];
      }

  for( i=1; i<MAP_WIDTH-1; i++ )
    {
    map->tile[ i ][ 1 ] = tile_backup[ i ];
    map->decoration[ i ][ 1 ] = deco_backup[ i ];
    }

  // Shift crates:
  for( i=0; i<map->crate_count; i++ )
    {
    map->crate[ i ].y += 1;

    if( map->crate[ i ].y > MAP_HEIGHT - 2 )
      map->crate[ i ].y = 1;
    }

  // Shift destinations:
  for( i=0; i<map->destination_count; i++ )
    {
    map->destination[ i ].y += 1;

    if( map->destination[ i ].y > MAP_HEIGHT - 2 )
      map->destination[ i ].y = 1;
    }

  // Shift player start position:
  if( map->start_y_pos != MAP_POSITION_DISABLED )
    {
    map->start_y_pos += 1;
    if( map->start_y_pos > MAP_HEIGHT - 2 )
      map->start_y_pos = 1;
    }

  fl_place_empty_tiles    ( map );
  fl_recalculate_blob_data( map );
  }

void map_shift_left ( struct s_map *map )
  {
  int i, j;
  int tile_backup[ MAP_HEIGHT ];
  int deco_backup[ MAP_HEIGHT ];

  assert( map );

  fl_remove_empty_tiles( map );

  for( j=1; j<MAP_HEIGHT-1; j++ )
    {
    tile_backup[ j ] = map->tile[ 1 ][ j ];
    deco_backup[ j ] = map->decoration[ 1 ][ j ];
    }

  for( i=1; i<MAP_WIDTH-1; i++ )
    for( j=1; j<MAP_HEIGHT-1; j++ )
      {
      map->tile[ i ][ j ] = map->tile[ i + 1 ][ j ];
      map->decoration[ i ][ j ] = map->decoration[ i + 1 ][ j ];
      }

  for( j=1; j<MAP_HEIGHT-1; j++ )
    {
    map->tile[ MAP_WIDTH - 2  ][ j ] = tile_backup[ j ];
    map->decoration[ MAP_WIDTH - 2 ][ j ] = deco_backup[ j ];
    }

  // Shift crates:
  for( i=0; i<map->crate_count; i++ )
    {
    map->crate[ i ].x -= 1;

    if( map->crate[ i ].x < 1 )
      map->crate[ i ].x = MAP_WIDTH - 2;
    }

  // Shift destinations:
  for( i=0; i<map->destination_count; i++ )
    {
    map->destination[ i ].x -= 1;

    if( map->destination[ i ].x < 1 )
      map->destination[ i ].x = MAP_WIDTH -2;
    }

  // Shift player start position:
  if( map->start_x_pos != MAP_POSITION_DISABLED )
    {
    map->start_x_pos -= 1;
    if( map->start_x_pos < 1 )
      map->start_x_pos = MAP_WIDTH - 2;
    }

  fl_place_empty_tiles    ( map );
  fl_recalculate_blob_data( map );
  }

void map_editor_place_random_decoration( struct s_map *map, int x, int y )
  {
  assert( map );

  if( fl_look_for_crate( map, x, y ) )
    return;

  if( fl_look_for_destination( map, x, y ) )
    return;

  if( fl_look_for_start_position( map, x, y ) )
    return;

  if( map->tile[ x ][ y ] == MAP_TILE_FLOOR )
    {
    if( map->decoration[ x ][ y ] == 0 )
      map->decoration[ x ][ y ] = ( rand() % MAP_MAX_DECORATIONS ) + 1;
    else
      map->decoration[ x ][ y ] = 0;
    }
  }

void map_editor_scroll_decoration( struct s_map *map, int x, int y, int z )
  {
  assert( map );

  if( fl_look_for_destination( map, x, y ) )
    return;

  if( fl_look_for_crate( map, x, y ) )
    return;

  if( fl_look_for_start_position( map, x, y ) )
    return;

  if( map->tile[ x ][ y ] == MAP_TILE_FLOOR )
    {
    map->decoration[ x ][ y ] += z;

    if( z < 0 )
      {
      if( map->decoration[ x ][ y ] < 0 )
        map->decoration[ x ][ y ] = MAP_MAX_DECORATIONS;
      }
    else
      {
      if( map->decoration[ x ][ y ] > MAP_MAX_DECORATIONS )
        map->decoration[ x ][ y ] = 0;
      }
    }
  }

void map_editor_rotate_start_direction( struct s_map *map, int x, int y, int z )
  {
  assert( map );

  if( fl_look_for_start_position( map, x, y ) )
    {
    map->start_direction += z;

    if( map->start_direction > 3 )
      map->start_direction = 0;

    if( map->start_direction < 0 )
      map->start_direction = 3;
    }
  }

bool map_has_start_position( struct s_map *map )
  {
  assert( map );

  if( map->start_x_pos == MAP_POSITION_DISABLED &&
      map->start_y_pos == MAP_POSITION_DISABLED )
    return false;

  return true;
  }

bool map_has_crates( struct s_map *map )
  {
  assert( map );

  if( map->crate_count == 0 )
    return false;

  return true;
  }

bool map_has_equal_amount_of_crates_and_destinations( struct s_map *map )
  {
  assert( map );

  if( map->crate_count != map->destination_count )
    return false;

  return true;
  }

bool map_is_start_position_on_coordinates( struct s_map *map, int x, int y )
  {
  assert( map );

  if( map->start_x_pos == x && map->start_y_pos == y )
    return true;

  return false;
  }

bool map_is_file_valid_map( const char *file_path )
  {
  ALLEGRO_FILE *file;
  char          file_header[ 8 ];

  file = NULL;
  file = al_fopen( file_path, "rb" );
  if( !file )
    return false;

  al_fread( file, file_header, MAP_LEVEL_FILE_HEADER_SIZE );
  file_header[ MAP_LEVEL_FILE_HEADER_SIZE ] = 0;

  al_fclose( file );

  if( strcmp( file_header, level_file_header ) != 0 )
    return false;

  return true;
  }

bool map_level_completed( struct s_map *map )
  {
  int i, j;
  bool crate_on_destination;

  assert( map );

  for( i=0; i<map->crate_count; i++ )
    {
    crate_on_destination = false;

    for( j=0; j<map->destination_count; j++ )
      if( map->crate[ i ].x == map->destination[ j ].x &&
          map->crate[ i ].y == map->destination[ j ].y )
        {
        crate_on_destination = true;
        break;
        }

    if( !crate_on_destination )
      return false;
    }

  return true;
  }

int map_get_start_x_pos( struct s_map *map )
  {
  assert( map );

  return map->start_x_pos;
  }

int map_get_start_y_pos( struct s_map *map )
  {
  assert( map );

  return map->start_y_pos;
  }

int map_get_start_direction( struct s_map *map )
  {
  assert( map );

  return map->start_direction;
  }

int map_get_crate_x_pos( struct s_map *map, int crate_id )
  {
  assert( map );

  return map->crate[ crate_id ].x;
  }

int map_get_crate_y_pos( struct s_map *map, int crate_id )
  {
  assert( map );

  return map->crate[ crate_id ].y;
  }

void map_set_crate_position( struct s_map *map,
                             int           crate_id,
                             int           crate_x,
                             int           crate_y )
  {
  assert( map );

  map->crate[ crate_id ].x = crate_x;
  map->crate[ crate_id ].y = crate_y;
  }
