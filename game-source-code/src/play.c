#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include "allegro5/allegro.h"
#include "allegro5/allegro_native_dialog.h"

#include "draw.h"
#include "logfile.h"
#include "play.h"
#include "struct_allegro.h"
#include "struct_config.h"
#include "struct_game.h"
#include "struct_map.h"
#include "struct_player.h"
#include "struct_resources.h"
#include "struct_user_interface.h"

enum
  {
  MOUSE_BUTTON_NONE = -1,
  MOUSE_BUTTON_1    =  1,
  MOUSE_BUTTON_2    =  2,
  MOUSE_BUTTON_3    =  3,

  MSGBOX_YES_BUTTON = 1,

  UNDO_MAX_STEPS = 100
  };

struct s_undo
  {
  int step;
  struct s_step
    {
    int px;
    int py;
    int pd;
    int cid;
    int cx;
    int cy;
    } step_data[ UNDO_MAX_STEPS ];
  };

static void fl_undo_clear        ( struct s_undo *undo );
static void fl_undo_save_step    ( struct s_undo *undo, int px, int py, int pd, int cid, int cx, int cy );
static bool fl_undo_can_step_back( struct s_undo *undo );
static void fl_undo_step_back    ( struct s_undo *undo );
static int  fl_undo_get_px       ( struct s_undo *undo );
static int  fl_undo_get_py       ( struct s_undo *undo );
static int  fl_undo_get_pd       ( struct s_undo *undo );
static int  fl_undo_get_cid      ( struct s_undo *undo );
static int  fl_undo_get_cx       ( struct s_undo *undo );
static int  fl_undo_get_cy       ( struct s_undo *undo );
static void fl_undo_one_step     ( struct s_map *map, struct s_player *plr, struct s_undo *undo );
static void fl_load_selected_map ( struct s_game *gam, struct s_map *map, struct s_player *plr );

static void fl_undo_clear( struct s_undo *undo )
  {
  assert( undo );

  undo->step = 0;
  }

static void fl_undo_save_step( struct s_undo *undo, int px, int py, int pd, int cid, int cx, int cy )
  {
  assert( undo );

  //Drop oldest saved step when we have reached max saved steps:
  if( undo->step == UNDO_MAX_STEPS )
    {
    int i;

    for( i=0; i<UNDO_MAX_STEPS - 1; i++ )
      {
      undo->step_data[ i ].px  = undo->step_data[ i + 1 ].px;
      undo->step_data[ i ].py  = undo->step_data[ i + 1 ].py;
      undo->step_data[ i ].pd  = undo->step_data[ i + 1 ].pd;
      undo->step_data[ i ].cid = undo->step_data[ i + 1 ].cid;
      undo->step_data[ i ].cx  = undo->step_data[ i + 1 ].cx;
      undo->step_data[ i ].cy  = undo->step_data[ i + 1 ].cy;
      }

    undo->step--;
    }

  // Save step data:
  undo->step_data[ undo->step ].px  = px;
  undo->step_data[ undo->step ].py  = py;
  undo->step_data[ undo->step ].pd  = pd;
  undo->step_data[ undo->step ].cid = cid;
  undo->step_data[ undo->step ].cx  = cx;
  undo->step_data[ undo->step ].cy  = cy;
  undo->step++;
  }

static bool fl_undo_can_step_back( struct s_undo *undo )
  {
  assert( undo );

  if( undo->step == 0 )
    return false;

  return true;
  }

static void fl_undo_step_back( struct s_undo *undo )
  {
  assert( undo );

  if( undo->step > 0 )
    undo->step--;
  }

static int fl_undo_get_px( struct s_undo *undo )
  {
  assert( undo );

  return undo->step_data[ undo->step ].px;
  }

static int fl_undo_get_py( struct s_undo *undo )
  {
  assert( undo );

  return undo->step_data[ undo->step ].py;
  }

static int fl_undo_get_pd( struct s_undo *undo )
  {
  assert( undo );

  return undo->step_data[ undo->step ].pd;
  }

static int fl_undo_get_cid( struct s_undo *undo )
  {
  assert( undo );

  return undo->step_data[ undo->step ].cid;
  }

static int fl_undo_get_cx( struct s_undo *undo )
  {
  assert( undo );

  return undo->step_data[ undo->step ].cx;
  }

static int fl_undo_get_cy( struct s_undo *undo )
  {
  assert( undo );

  return undo->step_data[ undo->step ].cy;
  }

static void fl_undo_one_step( struct s_map *map, struct s_player *plr, struct s_undo *undo )
  {
  assert( map );
  assert( plr );
  assert( undo );

  if( fl_undo_can_step_back( undo ) )
    {
    fl_undo_step_back( undo );

    plr_set_x( plr, fl_undo_get_px( undo ) );
    plr_set_y( plr, fl_undo_get_py( undo ) );
    plr_set_direction( plr, fl_undo_get_pd( undo ) );

    if( fl_undo_get_cid( undo ) != -1 )
      map_set_crate_position( map, fl_undo_get_cid( undo ),
                                   fl_undo_get_cx ( undo ),
                                   fl_undo_get_cy ( undo ) );
    }
  }

static void fl_load_selected_map( struct s_game *gam, struct s_map *map, struct s_player *plr )
  {
  const char *file_path;

  assert( gam );
  assert( map );
  assert( plr );

  file_path = gam_get_level_file_path( gam );

  if( map_load_map_from_file( map, file_path ) != MAP_LOAD_OK )
    {
    logfile_write( "PLAY: Error loading map." );
/*
    printf( "PLAY: Error loading map!\n" );
*/
    exit( 1 );
    }

  plr_set_x        ( plr, map_get_start_x_pos( map ) );
  plr_set_y        ( plr, map_get_start_y_pos( map ) );
  plr_set_direction( plr, map_get_start_direction( map ) );
  }

int game_play( struct s_allegro   *alg,
               struct s_config    *cfg,
               struct s_game      *gam,
               struct s_resources *res )
  {
  ALLEGRO_EVENT_QUEUE     *event_queue;
  ALLEGRO_EVENT            event;
  struct s_map             map;
  struct s_player          plr;
  struct s_user_interface  uin;
  struct s_user_interface  uin_pw;
  struct s_undo            undo;
  int                      scale;
  bool                     draw;
  bool                     pw_active;

  assert( alg );
  assert( cfg );
  assert( gam );
  assert( res );

  event_queue = alg_get_event_queue( alg );
  scale       = cfg_get_scale( cfg );
  draw        = false;
  pw_active   = false;

  uin_clear        ( &uin );
  uin_build_play_ui( &uin );

  uin_clear( &uin_pw );
  if( gam_get_game_type( gam ) == GAME_TYPE_PLAY )
    uin_build_pw_play( &uin_pw );
  else
    uin_build_pw_custom( &uin_pw );

  fl_load_selected_map( gam, &map, &plr );
  fl_undo_clear       ( &undo );

  al_clear_keyboard_state( NULL );
  al_flush_event_queue( event_queue );

  while( gam_get_state( gam ) == GAME_STATE_PLAY )
    {
    al_wait_for_event( event_queue, &event );

    switch( event.type )
      {
      case ALLEGRO_EVENT_DISPLAY_CLOSE:
        gam_set_state( gam, GAME_STATE_EXIT );
        break;

      case ALLEGRO_EVENT_KEY_DOWN:

        if( pw_active )
          {
          switch( event.keyboard.keycode )
            {
            case ALLEGRO_KEY_N:
            case ALLEGRO_KEY_ENTER:
              pw_active = false;
              gam_select_next_level( gam );
              fl_load_selected_map( gam, &map, &plr );
              fl_undo_clear( &undo );
              break;

            case ALLEGRO_KEY_M:
            case ALLEGRO_KEY_ESCAPE:
              gam_set_state( gam, GAME_STATE_MENU );
              break;
            }

          // Dont move if popup window is active, just break:
          break; // Breaks out of case ALLEGRO_EVENT_KEY_DOWN:;
          }

        // Undo:
        if( event.keyboard.keycode == ALLEGRO_KEY_U ||
            event.keyboard.keycode == ALLEGRO_KEY_BACKSPACE )
          fl_undo_one_step( &map, &plr, &undo );

        // Reset map:
        if( event.keyboard.keycode == ALLEGRO_KEY_R )
          {
          fl_load_selected_map( gam, &map, &plr );
          fl_undo_clear( &undo );
          }

        // Return to menu:
        if( event.keyboard.keycode == ALLEGRO_KEY_M ||
            event.keyboard.keycode == ALLEGRO_KEY_ESCAPE )
          gam_set_state( gam, GAME_STATE_MENU );

        // Player movement and crate pushing:
        if( ( event.keyboard.keycode == ALLEGRO_KEY_UP ||
              event.keyboard.keycode == ALLEGRO_KEY_DOWN ) ||
            ( event.keyboard.keycode == ALLEGRO_KEY_LEFT ||
              event.keyboard.keycode == ALLEGRO_KEY_RIGHT ) )
          {
          int px;
          int py;
          int x_direction;
          int y_direction;
          int direction;
          int tile_info[ 2 ];
          int crate_id;
          int old_pd;
          int old_px;
          int old_py;
          int old_cid;
          int old_cx;
          int old_cy;

          old_pd = plr_get_direction( &plr );
          old_px = plr_get_x( &plr );
          old_py = plr_get_y( &plr );
          old_cid = -1;
          old_cx  = -1;
          old_cy  = -1;

          switch( event.keyboard.keycode )
            {
            case ALLEGRO_KEY_UP:
              x_direction =  0;
              y_direction = -1;
              direction = 0;
              break;

            case ALLEGRO_KEY_RIGHT:
              x_direction = 1;
              y_direction = 0;
              direction = 1;
              break;

            case ALLEGRO_KEY_DOWN:
              x_direction = 0;
              y_direction = 1;
              direction = 2;
              break;

            case ALLEGRO_KEY_LEFT:
              x_direction = -1;
              y_direction =  0;
              direction = 3;
              break;
            }

          plr_set_direction( &plr, direction );

          px = plr_get_x( &plr );
          py = plr_get_y( &plr );

          tile_info[ 0 ] = map_get_tile_info( &map,
                                              px + x_direction,
                                              py + y_direction );

          tile_info[ 1 ] = map_get_tile_info( &map,
                                              px + ( x_direction * 2 ),
                                              py + ( y_direction * 2 ) );

          if( tile_info[ 0 ] == MAP_TILE_INFO_CRATE &&
              tile_info[ 1 ] == MAP_TILE_INFO_CAN_WALK )
            {
            crate_id = map_get_crate_id( &map,
                                         px + x_direction,
                                         py + y_direction );

            // Save crate id and position before moving:
            old_cid = crate_id;
            old_cx  = map_get_crate_x_pos( &map, old_cid );
            old_cy  = map_get_crate_y_pos( &map, old_cid );

            map_move_crate( &map, crate_id, x_direction, y_direction );
            plr_move( &plr, x_direction, y_direction );

            if( map_level_completed( &map ) )
              {
              if( gam_get_game_type( gam ) == GAME_TYPE_PLAY )
                {
                gam_set_current_level_status( gam, GAME_LEVEL_STATUS_COMPLETED );
                gam_keep_three_levels_aviable( gam );
                gam_save_progress( gam, "game.sav" );
                }

              pw_active = true;
              }
            }
          else
            {
            if( tile_info[ 0 ] == MAP_TILE_INFO_CAN_WALK )
              {
              plr_move( &plr, x_direction, y_direction );
              }
            }

          if( old_pd != plr_get_direction( &plr ) ||
              old_px != plr_get_x( &plr ) ||
              old_py != plr_get_y( &plr ) )
            fl_undo_save_step( &undo, old_px, old_py, old_pd, old_cid, old_cx, old_cy );
          }
        break;

      case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
        if( event.mouse.button == MOUSE_BUTTON_1 )
          {
          int mx, my;
          int eid;

          mx = event.mouse.x / scale;
          my = event.mouse.y / scale;

          if( pw_active )
            eid = uin_check_mouse_click( &uin_pw, mx, my );
          else
            eid = uin_check_mouse_click( &uin, mx, my );

          switch( eid )
            {
            case UIE_P_BUTTON_MENU:
            case UIE_P_PW_BUTTON_MENU:
              gam_set_state( gam, GAME_STATE_MENU );
              break;

            case UIE_P_BUTTON_RESET:
              fl_load_selected_map( gam, &map, &plr );
              fl_undo_clear( &undo );
              break;

            case UIE_P_BUTTON_UNDO:
              fl_undo_one_step( &map, &plr, &undo );
              break;

            case UIE_P_PW_BUTTON_NEXT_LEVEL:
              pw_active = false;
              gam_select_next_level( gam );
              fl_load_selected_map( gam, &map, &plr );
              break;
            }
          }
        break;

      case ALLEGRO_EVENT_TIMER:
        draw = true;
        break;
      }

    if( draw && al_is_event_queue_empty( event_queue ) )
      {
      draw = false;

      al_hold_bitmap_drawing( true );

      draw_map   ( &map, res, true, true, false );
      draw_player( &plr, res );
      draw_ui    ( res, &uin );
      draw_level_number_text( gam, res );

      if( pw_active )
        {
        draw_popup_window( res );
        draw_ui( res, &uin_pw );
        }

      al_hold_bitmap_drawing( false );

      al_flip_display();
      }
    }

  return 0;
  }
