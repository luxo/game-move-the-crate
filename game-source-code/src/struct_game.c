#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "allegro5/allegro.h"

#include "logfile.h"
#include "struct_game.h"

enum
  {
  GAM_PROGRESS_FILE_HEADE_SIZE = 6
  };

const char progress_file_header[] = "PTCSAV";

static void fl_lock_all_levels          ( struct s_game *gam );
static void fl_keep_three_levels_aviable( struct s_game *gam );

static void fl_lock_all_levels( struct s_game *gam )
  {
  int i;

  assert( gam );

  for( i=0; i<GAME_LEVEL_COUNT; i++ )
    gam->level[ i ].status = GAME_LEVEL_STATUS_LOCKED;
  }

static void fl_keep_three_levels_aviable( struct s_game *gam )
  {
  int i;
  int lca; // Level count aviable

  assert( gam );

  lca = 0;

  for( i=0; i<GAME_LEVEL_COUNT; i++ )
    if( gam->level[ i ].status == GAME_LEVEL_STATUS_AVIABLE )
      lca++;

  if( lca < 3 )
    for( i=0; i<GAME_LEVEL_COUNT; i++ )
      if( gam->level[ i ].status == GAME_LEVEL_STATUS_LOCKED )
        {
        gam->level[ i ].status = GAME_LEVEL_STATUS_AVIABLE;
        lca++;
        if( lca == 3 )
          break;
        }
  }

struct s_game *gam_create_game_struct()
  {
  struct s_game *gam;

  gam = NULL;
  gam = malloc( sizeof( struct s_game ) );
  if( !gam )
    {
    logfile_write( "GAM: Failed to allocate memory for game struct." );
/*
    printf( "GAM: Failed to create game struct!\n" );
*/
    return NULL;
    }

  // Default values:
  gam->state          = GAME_STATE_MENU;
  gam->game_type      = GAME_TYPE_PLAY;
  gam->page           = 0;
  gam->selected_level = 0;

  fl_lock_all_levels          ( gam );
  fl_keep_three_levels_aviable( gam );

  return gam;
  }

void gam_destroy_game_struct( struct s_game *gam )
  {
  assert( gam );

  free( gam );
  }

void gam_keep_three_levels_aviable( struct s_game *gam )
  {
  assert( gam );

  fl_keep_three_levels_aviable( gam );
  }

void gam_select_next_level( struct s_game *gam )
  {
  int current_level;

  assert( gam );

  current_level = ( gam->page * GAME_LEVELS_PER_PAGE ) + gam->selected_level;

  if( current_level >= ( GAME_LEVEL_COUNT - 1 ) )
    return;

  gam->selected_level++;

  if( gam->selected_level >= GAME_LEVELS_PER_PAGE )
    {
    gam->selected_level = 0;
    gam->page++;
    }

  sprintf( gam->level_path,
           "data/levels/level%03d.lvl",
           ( gam->page * GAME_LEVELS_PER_PAGE ) + gam->selected_level + 1 );
  }

void gam_reset_progress( struct s_game *gam )
  {
  assert( gam );

  fl_lock_all_levels          ( gam );
  fl_keep_three_levels_aviable( gam );
  }

int gam_save_progress( struct s_game *gam, const char *file_path )
  {
  ALLEGRO_FILE *f;
  int           i;

  assert( gam );

  f = NULL;
  f = al_fopen( file_path, "wb" );
  if( !f )
    return -1;

  al_fwrite( f, progress_file_header, GAM_PROGRESS_FILE_HEADE_SIZE );

  for( i=0; i<GAME_LEVEL_COUNT; i++ )
    al_fwrite( f, &gam->level[ i ].status, 1 );

  al_fclose( f );

  return 0;
  }

int gam_load_progress( struct s_game *gam, const char *file_path )
  {
  ALLEGRO_FILE *f;
  char          buff[ 8 ];
  int           i;

  assert( gam );

  f = NULL;
  f = al_fopen( file_path, "rb" );
  if( !f )
    return -1;

  al_fread( f, buff, GAM_PROGRESS_FILE_HEADE_SIZE );
  buff[ GAM_PROGRESS_FILE_HEADE_SIZE ] = 0;
  if( strcmp( buff, progress_file_header ) != 0 )
    {
    al_fclose( f );
    return -2;
    }

  for( i=0; i<GAME_LEVEL_COUNT; i++ )
    al_fread( f, &gam->level[ i ].status, 1 );

  al_fclose( f );

  return 0;
  }

void gam_page_plus ( struct s_game *gam )
  {
  assert( gam );

  if( gam->page < GAME_PAGE_COUNT - 1 )
    gam->page++;
  }

void gam_page_minus( struct s_game *gam )
  {
  assert( gam );

  if( gam->page > 0 )
    gam->page--;
  }

void gam_set_game_type ( struct s_game *gam, int new_game_type )
  {
  assert( gam );
  assert( new_game_type >= GAME_TYPE_PLAY && new_game_type <= GAME_TYPE_CUSTOM );

  gam->game_type = new_game_type;
  }

void gam_set_state( struct s_game *gam, int new_state )
  {
  assert( gam );
  assert( new_state >= GAME_STATE_MENU && new_state <= GAME_STATE_EXIT );

  gam->state = new_state;
  }

void gam_set_level_path( struct s_game *gam, const char *path )
  {
  assert( gam );

  strcpy( gam->level_path, path );
  }

void gam_set_selected_level( struct s_game *gam, int new_selected_level )
  {
  assert( gam );

  gam->selected_level = new_selected_level;
  }

void gam_set_current_level_status( struct s_game *gam, int new_status )
  {
  int cl; // Current Level

  assert( gam );

  cl = ( gam->page * GAME_LEVELS_PER_PAGE ) + gam->selected_level;

  gam->level[ cl ].status = new_status;
  }

int gam_get_page( struct s_game *gam )
  {
  assert( gam );

  return gam->page;
  }

int gam_get_game_type( struct s_game *gam )
  {
  assert( gam );

  return gam->game_type;
  }

int gam_get_selected_level( struct s_game *gam )
  {
  assert( gam );

  return gam->selected_level;
  }

int gam_get_state( struct s_game *gam )
  {
  assert( gam );

  return gam->state;
  }

int gam_get_selected_level_status( struct s_game *gam )
  {
  assert( gam );

  return gam->level[ ( gam->page * GAME_LEVELS_PER_PAGE ) +
                       gam->selected_level ].status;
  }

const char *gam_get_level_file_path ( struct s_game *gam )
  {
  assert( gam );

  return gam->level_path;
  }
