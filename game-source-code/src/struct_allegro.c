#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include "allegro5/allegro.h"
#include "allegro5/allegro_font.h"
#include "allegro5/allegro_image.h"
#include "allegro5/allegro_native_dialog.h"

#include "logfile.h"
#include "struct_allegro.h"

// Creates and return framework structure:
struct s_allegro *alg_create_struct()
  {
  struct s_allegro *alg;

  alg = NULL;
  alg = malloc( sizeof( struct s_allegro ) );
  if( !alg )
    {
    logfile_write( "ALG: Failed to allocate memory for allegro structure." );
/*
    printf( "ALG: Failed to allocate memory for allegro structure!\n" );
*/
    return NULL;
    }

  // Set default values:
  alg->display        = NULL;
  alg->event_queue    = NULL;
  alg->timer          = NULL;
  alg->display_flags  = ALLEGRO_WINDOWED | ALLEGRO_GENERATE_EXPOSE_EVENTS;
  alg->display_width  = 640;
  alg->display_height = 480;
  alg->buffer_width   = 640;
  alg->buffer_height  = 480;
  alg->timer_speed    = 1.0 / 60.0;
  alg->initialized    = false;

  return alg;
  }

// Destroy framework struct:
void alg_destroy_struct( struct s_allegro *alg )
  {
  assert( alg );

  alg_destroy_all_internal_data( alg );

  free( alg );
  }

void alg_destroy_all_internal_data( struct s_allegro *alg )
  {
  assert( alg );

  if( alg->event_queue )
    {
    al_destroy_event_queue( alg->event_queue );
    alg->event_queue = NULL;
    }

  if( alg->timer )
    {
    al_destroy_timer( alg->timer );
    alg->timer = NULL;
    }

  if( alg->display )
    {
    al_destroy_display( alg->display );
    alg->timer = NULL;
    }

  alg->initialized = false;

  al_uninstall_mouse();
  al_uninstall_keyboard();
  al_shutdown_native_dialog_addon();
  al_shutdown_font_addon();
  al_shutdown_image_addon();
  al_uninstall_system();
  }

// Set window type and flags:
void alg_set_display_flags( struct s_allegro *alg, int flags )
  {
  assert( alg );
  assert( alg->initialized == false );

  alg->display_flags = flags;
  }

// Set display resolution:
void alg_set_display_resolution( struct s_allegro *alg, int w, int h )
  {
  assert( alg );
  assert( alg->initialized == false );

  if( ( w >= ALG_RESOLUTION_MIN_W && w <= ALG_RESOLUTION_MAX_W ) &&
      ( h >= ALG_RESOLUTION_MIN_H && h <= ALG_RESOLUTION_MAX_H ) )
    {
    alg->display_width  = w;
    alg->display_height = h;
    }
  else
    {
    logfile_write( "ALG: Display resolution out of bounds. Using default values ( 640x480 )." );
/*
    printf( "ALG: Display resolution out of bounds! Using default values!\n" );
*/
    alg->display_width  = 640;
    alg->display_height = 480;
    }
  }

// Set buffer resolution:
void alg_set_buffer_resolution( struct s_allegro *alg, int w, int h )
  {
  assert( alg );
  assert( alg->initialized == false );

  if( ( w >= ALG_RESOLUTION_MIN_W && w <= ALG_RESOLUTION_MAX_W ) &&
      ( h >= ALG_RESOLUTION_MIN_H && h <= ALG_RESOLUTION_MAX_H ) )
    {
    alg->buffer_width  = w;
    alg->buffer_height = h;
    }
  else
    {
    logfile_write( "ALG: Buffer resolution out of bounds. Using default values ( 640x480 )." );
/*
    printf( "ALG: Buffer resolution out of bounds! Using default values!\n" );
*/
    alg->buffer_width  = 640;
    alg->buffer_height = 480;
    }
  }

// Set timer speed:
// TODO: Do some timer range limit settigs!
void alg_set_timer_speed( struct s_allegro *alg, double ts )
  {
  assert( alg );
  assert( alg->initialized == false );

  alg->timer_speed = ts;
  }

// Initialization:
int alg_initialize( struct s_allegro *alg )
  {
  ALLEGRO_EVENT_SOURCE *es;

  assert( alg );
  assert( alg->initialized == false );

  if( !al_init() )
    {
    logfile_write( "ALG: al_init() failed." );
/*
    printf( "ALG: al_init() failed !\n" );
*/
    return -1;
    }

  if( !al_init_image_addon() )
    {
    logfile_write( "ALG: al_init_image_addon() failed." );
/*
    printf( "ALG: al_init_image_addon() failed !\n" );
*/
    return -1;
    }

  if( !al_init_font_addon() )
    {
    logfile_write( "ALG: al_init_font_addon() failed." );
/*
    printf( "ALG: al_init_font_addon() failed !\n" );
*/
    return -1;
    }

  if( !al_init_native_dialog_addon() )
    {
    logfile_write( "ALG: al_init_native_dialog_addon() failed." );
/*
    printf( "ALG: al_init_native_dialog_addon() failed !\n" );
*/
    return -1;
    }

  if( !al_install_keyboard() )
    {
    logfile_write( "ALG: al_install_keyboard() failed." );
/*
    printf( "ALG: al_install_keyboard() failed !\n" );
*/
    return -1;
    }

  if( !al_install_mouse() )
    {
    logfile_write( "ALG: al_install_mouse() failed." );
/*
    printf( "ALG: al_install_mouse() failed !\n" );
*/
    return -1;
    }

  al_set_new_display_flags( alg->display_flags );
  alg->display = al_create_display( alg->display_width, alg->display_height );
  if( !alg->display )
    {
    logfile_write( "ALG: Failed to create display." );
/*
    printf( "ALG: Failed to create display !\n" );
*/
    return -1;
    }

  // If the display was created with ALLEGRO_FULLSCREEN_WINDOW flag,
  // get the display width and height to make sure they are accurate:
  if( alg->display_flags & ALLEGRO_FULLSCREEN_WINDOW )
    {
    alg->display_width  = al_get_display_width ( alg->display );
    alg->display_height = al_get_display_height( alg->display );
    }

  alg->timer = al_create_timer( alg->timer_speed );
  if( !alg->timer )
    {
    logfile_write( "ALG: Failed to create timer." );
/*
    printf( "ALG: Failed to create timer!\n");
*/
    return -1;
    }

  alg->event_queue = al_create_event_queue();
  if( !alg->event_queue )
    {
    logfile_write( "ALG: Failed to create event queue." );
/*
    printf( "ALG: Failed to create event queue !\n" );
*/
    return -1;
    }

  es = NULL;
  es = al_get_display_event_source( alg->display );
  if( !es )
    {
    logfile_write( "ALG: Failed to get display event source." );
/*
    printf( "ALG: Failed to get display event source!\n");
*/
    return -1;
    }
  al_register_event_source( alg->event_queue, es );

  es = NULL;
  es = al_get_timer_event_source( alg->timer );
  if( !es )
    {
    logfile_write( "ALG: Failed to get timer event source." );
/*
    printf( "ALG: Failed to get timer event source!\n");
*/
    return -1;
    }
  al_register_event_source( alg->event_queue, es );

  es = NULL;
  es = al_get_keyboard_event_source();
  if( !es )
    {
    logfile_write( "ALG: Failed to get keyboard event source." );
/*
    printf( "ALG: Failed to get keyboard event source!\n");
*/
    return -1;
    }
  al_register_event_source( alg->event_queue, es );

  es = NULL;
  es = al_get_mouse_event_source();
  if( !es )
    {
    logfile_write( "ALG: Failed to get mouse event source." );
/*
    printf( "ALG: Failed to get mouse event source!\n");
*/
    return -1;
    }
  al_register_event_source( alg->event_queue, es );

  al_start_timer( alg->timer );

  al_flush_event_queue( alg->event_queue );

  alg->initialized = true;
  return 0;
  }

// Scale screen to display and keep aspect ratio:
void alg_scale_buffer_to_display( struct s_allegro *alg )
  {
  ALLEGRO_TRANSFORM t;
  float display_w, display_h;
  float buffer_w, buffer_h;
  float offset_x, offset_y;
  float scale_w, scale_h;
  float scale;

  assert( alg );
  assert( alg->initialized == true );

  display_w = alg->display_width;
  display_h = alg->display_height;

  buffer_w = alg->buffer_width;
  buffer_h = alg->buffer_height;

  scale_w = display_w / buffer_w;
  scale_h = display_h / buffer_h;

  if( scale_w < scale_h )
    scale = scale_w;
  else
    scale = scale_h;

  offset_x = ( display_w - ( buffer_w * scale ) ) / 2.0;
  offset_y = ( display_h - ( buffer_h * scale ) ) / 2.0;

  al_identity_transform ( &t );
  al_scale_transform    ( &t, scale, scale );
  al_translate_transform( &t, offset_x, offset_y );
  al_use_transform      ( &t );

  al_set_clipping_rectangle( offset_x, offset_y,
                             buffer_w * scale, buffer_h * scale );
  }

// Start timer:
void alg_start_timer( struct s_allegro *alg )
  {
  assert( alg );
  assert( alg->initialized == true );

  if( alg->timer )
    al_start_timer( alg->timer );
  }

// Stop timer:
void alg_stop_timer( struct s_allegro *alg )
  {
  assert( alg );
  assert( alg->initialized == true );

  if( alg->timer )
    al_stop_timer( alg->timer );
  }

void alg_set_window_title( struct s_allegro *alg, const char *window_title )
  {
  assert( alg );
  assert( alg->initialized == true );

  al_set_window_title( alg->display, window_title );
  }

void alg_set_display_icon( struct s_allegro *alg, ALLEGRO_BITMAP *bmp )
  {
  assert( alg );
  assert( bmp );

  al_set_display_icon( alg->display, bmp );
  }

// Getters:
ALLEGRO_DISPLAY *alg_get_display( struct s_allegro *alg )
  {
  assert( alg );
  assert( alg->initialized == true );

  return alg->display;
  }

ALLEGRO_EVENT_QUEUE *alg_get_event_queue( struct s_allegro *alg )
  {
  assert( alg );
  assert( alg->initialized == true );

  return alg->event_queue;
  }
