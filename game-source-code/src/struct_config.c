#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "logfile.h"
#include "struct_config.h"

struct s_config *cfg_create_struct()
  {
  struct s_config *cfg;

  cfg = NULL;
  cfg = malloc( sizeof( struct s_config ) );
  if( !cfg )
    {
    logfile_write( "CFG: Failed to allocate memory for config struct." );
/*
    printf( "CFG: Failed to allocate memory for config struct!\n" );
*/
    return NULL;
    }

  // Default values:
  cfg->scale = CFG_SCALE_1;

  return cfg;
  }

void cfg_destroy_struct( struct s_config *cfg )
  {
  assert( cfg );

  free( cfg );
  }

void cfg_set_default_values( struct s_config *cfg )
  {
  assert( cfg );

  cfg->scale = CFG_SCALE_1;
  }

int cfg_load_from_file( struct s_config * cfg, const char *file_path )
  {
  FILE *f;
  char file_header[ 7 ] = "PTCCFG";
  char buff[ 7 ];

  assert( cfg );

  f = NULL;
  f = fopen( file_path, "rb" );
  if( !f )
    {
    logfile_write( "CFG: Failed to open config file." );
/*
    printf( "CFG: Failed to open config file!\n" );
*/
    return -1;
    }

  memset( buff, 0, 7 );

  fread( buff, 1, 6, f );

  if( strcmp( file_header, buff ) != 0 )
    {
    char str_file_path[ 1024 ];

    sprintf( str_file_path, "     FILE = %s", file_path );

    logfile_write( "CFG: Not a config file." );
    logfile_write( str_file_path );
/*
    printf( "CFG: Not a config file!\n" );
    printf( "PATH = %s\n", file_path );
*/
    return -1;
    }

  fread( &cfg->scale, 1, sizeof( cfg->scale ), f );

  fclose( f );

  return 0;
  }

int cfg_save_to_file( struct s_config *cfg, const char *file_path )
  {
  FILE *f;
  char file_header[ 7 ] = "PTCCFG";

  assert( cfg );

  f = NULL;
  f = fopen( file_path, "wb" );
  if( !f )
    {
    char str_file_path[ 1024 ];

    sprintf( str_file_path, "     FILE = %s", file_path );

    logfile_write( "CFG: Failed to open file for saving config." );
    logfile_write( str_file_path );
/*
    printf( "CFG: Failed to open file for saving config!\n" );
    printf( "PATH = %s\n", file_path );
*/
    return -1;
    }

  fwrite( file_header, 1, 6                   , f );
  fwrite( &cfg->scale, 1, sizeof( cfg->scale ), f );

  fclose( f );

  return 0;
  }

int cfg_get_scale( struct s_config *cfg )
  {
  assert( cfg );

  return cfg->scale;
  }

void cfg_set_scale( struct s_config *cfg, int new_scale )
  {
  assert( cfg );

  cfg->scale = new_scale;
  }
