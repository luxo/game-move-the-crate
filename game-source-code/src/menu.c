#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include "allegro5/allegro.h"
#include "allegro5/allegro_native_dialog.h"

#include "draw.h"
#include "menu.h"
#include "struct_allegro.h"
#include "struct_config.h"
#include "struct_game.h"
#include "struct_map.h"
#include "struct_resources.h"
#include "struct_user_interface.h"

enum
  {
  MOUSE_BUTTON_1    =  1,

  MSGBOX_YES_BUTTON = 1,
  MSGBOX_NO_BUTTON  = 2
  };

static void fl_play_custom_level  ( struct s_allegro *alg, struct s_game *gam );
static void fl_play_selected_level( struct s_game *gam );
static void fl_reset_progress     ( struct s_allegro *alg, struct s_game *gam );
static void fl_resize_windnow     ( struct s_allegro   *alg,
                                    struct s_config    *cfg,
                                    struct s_resources *res,
                                    int                 new_scale );

static void fl_play_custom_level( struct s_allegro *alg, struct s_game *gam )
  {
  ALLEGRO_DISPLAY     *display;
  ALLEGRO_FILECHOOSER *dialog;
  char                 file_path[ 1024 ];

  assert( alg );
  assert( gam );

  display = alg_get_display( alg );
  dialog  = NULL;

  dialog = al_create_native_file_dialog( "",
                                         "Load custom map:",
                                         "",
                                         ALLEGRO_FILECHOOSER_FILE_MUST_EXIST );

  if( dialog )
    {
    al_show_native_file_dialog( display, dialog );

    if( al_get_native_file_dialog_count( dialog ) > 0 )
      strcpy( file_path, al_get_native_file_dialog_path( dialog, 0 ) );
    else
      return; // File selection has been canceled.

    al_destroy_native_file_dialog( dialog );
    }
  else
    {
    al_show_native_message_box( display,
                                "Load custom map",
                                "Could not load custom map !",
                                "Failed to create file selection dialog !",
                                NULL,
                                ALLEGRO_MESSAGEBOX_ERROR );
    return;
    }

  if( !map_is_file_valid_map( file_path ) )
    {
    al_show_native_message_box( display,
                                "Load custom map",
                                "Could not load custom map !",
                                "Selected file is not a map file !",
                                NULL,
                                ALLEGRO_MESSAGEBOX_ERROR );
    return;
    }

  gam_set_level_path( gam, file_path );
  gam_set_game_type ( gam, GAME_TYPE_CUSTOM );
  gam_set_state     ( gam, GAME_STATE_PLAY );
  }

static void fl_play_selected_level( struct s_game *gam )
  {
  int  page;
  int  selected_level;
  char file_path[ 32 ];

  assert( gam );

  // Cannot play locked level:
  if( gam_get_selected_level_status( gam ) == GAME_LEVEL_STATUS_LOCKED )
    return;

  page           = gam_get_page( gam );
  selected_level = gam_get_selected_level( gam );

  sprintf( file_path,
           "data/levels/level%03d.lvl",
           ( page * GAME_LEVELS_PER_PAGE ) + selected_level + 1 );

  gam_set_level_path( gam, file_path );
  gam_set_game_type ( gam, GAME_TYPE_PLAY );
  gam_set_state     ( gam, GAME_STATE_PLAY );
  }

static void fl_reset_progress( struct s_allegro *alg, struct s_game *gam )
  {
  ALLEGRO_DISPLAY *display;
  int              rval;

  assert( alg );
  assert( gam );

  display = alg_get_display( alg );

  rval = al_show_native_message_box( display,
                                     "Reset progress",
                                     "Realy reset the game progress ?",
                                     "You will have to play all levels again.",
                                     NULL,
                                     ALLEGRO_MESSAGEBOX_YES_NO );

  if( rval == MSGBOX_YES_BUTTON )
    {
    gam_reset_progress( gam );
    gam_save_progress ( gam, "game.sav" );
    }
  }

static void fl_resize_windnow( struct s_allegro   *alg,
                               struct s_config    *cfg,
                               struct s_resources *res,
                               int                 new_scale )
  {
  ALLEGRO_DISPLAY *display;
  int              ns; // New Scale
  int              os; // Old Scale
  int              rval;
  bool             repeat;

  assert( alg );
  assert( cfg );
  assert( res );

  repeat = true;
  ns     = new_scale;
  os     = cfg_get_scale( cfg );

  if( os != ns )
    {

    while( repeat )
      {
      res_destroy_all_internal_data( res );
      alg_destroy_all_internal_data( alg );

      cfg_set_scale      ( cfg, ns );
      cfg_save_to_file   ( cfg, "config.cfg" );

      alg_set_display_flags     ( alg, ALLEGRO_WINDOWED | ALLEGRO_GENERATE_EXPOSE_EVENTS );
      alg_set_buffer_resolution ( alg, 640, 480 );
      alg_set_display_resolution( alg, 640 * ns, 480 * ns );
      alg_set_timer_speed       ( alg, 1.0 / 60.0 );

      if( alg_initialize( alg ) == -1 )
        exit( -1 );

      alg_scale_buffer_to_display( alg );
      alg_set_window_title       ( alg, "Move the crate" );

      res_initialize    ( res );
      res_load_resources( res );

      alg_set_display_icon( alg, res_get_sprite( res, SPR_CRATE ) );

      repeat = false;

      if( os != ns )
        {
        display = alg_get_display( alg );
        rval = al_show_native_message_box( display,
                                           "Resize window",
                                           "Keep current window size?",
                                           "",
                                           NULL,
                                           ALLEGRO_MESSAGEBOX_YES_NO );

        if( rval == MSGBOX_NO_BUTTON )
          {
          repeat = true;
          ns = os;
          }
        }
      }
    }
  }

int game_menu( struct s_allegro   *alg,
               struct s_config    *cfg,
               struct s_game      *gam,
               struct s_resources *res )
  {
  ALLEGRO_EVENT_QUEUE     *event_queue;
  ALLEGRO_EVENT            event;
  struct s_user_interface  uin;
  bool                     draw;
  int                      scale;
  int                      selected_level;

  assert( alg );
  assert( cfg );
  assert( gam );
  assert( res );

  uin_clear     ( &uin );
  uin_build_menu( &uin );

  scale       = cfg_get_scale( cfg );
  event_queue = alg_get_event_queue( alg );
  draw        = false;

  al_flush_event_queue( event_queue );

  while( gam_get_state( gam ) == GAME_STATE_MENU )
    {
    al_wait_for_event( event_queue, &event );

    switch( event.type )
      {
      case ALLEGRO_EVENT_DISPLAY_CLOSE:
        gam_set_state( gam, GAME_STATE_EXIT );
        break;

      case ALLEGRO_EVENT_KEY_DOWN:
        switch( event.keyboard.keycode )
          {
          case ALLEGRO_KEY_ESCAPE:
            gam_set_state( gam, GAME_STATE_EXIT );
            break;

          case ALLEGRO_KEY_P:
          case ALLEGRO_KEY_ENTER:
            fl_play_selected_level( gam );
            break;

          case ALLEGRO_KEY_C:
            fl_play_custom_level( alg, gam );
            al_flush_event_queue( event_queue );
            break;

          case ALLEGRO_KEY_E:
            gam_set_state( gam, GAME_STATE_EDITOR );
            break;

          case ALLEGRO_KEY_H:
            gam_set_state( gam, GAME_STATE_MENU_HELP );
            break;

          case ALLEGRO_KEY_S:
            gam_set_state( gam, GAME_STATE_MENU_SETTINGS );
            break;

          case ALLEGRO_KEY_PGUP:
            gam_page_plus( gam );
            break;

          case ALLEGRO_KEY_PGDN:
            gam_page_minus( gam );
            break;

          case ALLEGRO_KEY_RIGHT:
            selected_level = gam_get_selected_level( gam );

            if( selected_level != 4 && selected_level != 9 )
              {
              gam_set_selected_level( gam, selected_level + 1 );
              break;
              }

            if( selected_level == 4 || selected_level == 9 )
              if( gam_get_page( gam ) < GAME_PAGE_COUNT - 1 )
                {
                gam_page_plus( gam );
                gam_set_selected_level( gam, selected_level - 4 );
                }
            break;

          case ALLEGRO_KEY_LEFT:
            selected_level = gam_get_selected_level( gam );

            if( selected_level != 0 && selected_level != 5 )
              {
              gam_set_selected_level( gam, selected_level - 1 );
              break;
              }

            if( selected_level == 0 || selected_level == 5 )
              if( gam_get_page( gam ) > 0 )
                {
                gam_page_minus( gam );
                gam_set_selected_level( gam, selected_level + 4 );
                }
            break;

          case ALLEGRO_KEY_DOWN:
            selected_level = gam_get_selected_level( gam );

            if( selected_level <= 4 )
              gam_set_selected_level( gam, selected_level + 5 );
            break;

          case ALLEGRO_KEY_UP:
            selected_level =  gam_get_selected_level( gam );

            if( selected_level > 4 )
              gam_set_selected_level( gam, selected_level - 5 );
            break;
          }
        break;

      case ALLEGRO_EVENT_TIMER:
        draw = true;
        break;

      case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
        if( event.mouse.button == MOUSE_BUTTON_1 )
          {
          int mx, my;
          int eid;

          mx = event.mouse.x / scale;
          my = event.mouse.y / scale;

          eid = uin_check_mouse_click( &uin, mx, my );

          switch( eid )
            {
            case UIE_M_BUTTON_PAGE_MINUS:
              gam_page_minus( gam );
              break;

            case UIE_M_BUTTON_PAGE_PLUS:
              gam_page_plus( gam );
              break;

            case UIE_M_BUTTON_PLAY:
              fl_play_selected_level( gam );
              break;

            case UIE_M_BUTTON_CONFIG:
              gam_set_state( gam, GAME_STATE_MENU_SETTINGS );
              break;

            case UIE_M_BUTTON_EXIT:
              gam_set_state( gam, GAME_STATE_EXIT );
              break;

            case UIE_M_BUTTON_EDITOR:
              gam_set_state( gam, GAME_STATE_EDITOR );
              break;

            case UIE_M_BUTTON_CUSTOM:
              fl_play_custom_level( alg, gam );
              al_flush_event_queue( event_queue );
              break;

            case UIE_M_BUTTON_HELP:
              gam_set_state( gam, GAME_STATE_MENU_HELP );
              break;

            case UIE_M_BUTTON_LEVEL_0:
            case UIE_M_BUTTON_LEVEL_1:
            case UIE_M_BUTTON_LEVEL_2:
            case UIE_M_BUTTON_LEVEL_3:
            case UIE_M_BUTTON_LEVEL_4:
            case UIE_M_BUTTON_LEVEL_5:
            case UIE_M_BUTTON_LEVEL_6:
            case UIE_M_BUTTON_LEVEL_7:
            case UIE_M_BUTTON_LEVEL_8:
            case UIE_M_BUTTON_LEVEL_9:
              gam_set_selected_level( gam, eid );
              break;
            }
          }
        break;
      }

    if( draw && al_is_event_queue_empty( event_queue ) )
      {
      draw = false;

      al_hold_bitmap_drawing( true );

      al_clear_to_color        ( al_map_rgb( 0, 0, 0 ) );
      draw_ui                  ( res, &uin );
      draw_menu_level_selection( gam, res );

      al_hold_bitmap_drawing( false );

      al_flip_display();
      }
    }

  return 0;
  }

int game_menu_settings( struct s_allegro   *alg,
                        struct s_config    *cfg,
                        struct s_game      *gam,
                        struct s_resources *res )
  {
  ALLEGRO_EVENT_QUEUE    *event_queue;
  ALLEGRO_EVENT           event;
  struct s_user_interface uin;
  bool                    draw;
  int                     scale;
  int                     new_scale;

  assert( alg );
  assert( cfg );
  assert( gam );
  assert( res );

  uin_clear              ( &uin );
  uin_build_menu_settings( &uin );

  scale       = cfg_get_scale      ( cfg );
  event_queue = alg_get_event_queue( alg );
  new_scale   = cfg_get_scale      ( cfg );
  draw        = false;

  while( gam_get_state( gam ) == GAME_STATE_MENU_SETTINGS )
    {
    al_wait_for_event( event_queue, &event );

    switch( event.type )
      {
      case ALLEGRO_EVENT_DISPLAY_CLOSE:
        gam_set_state( gam, GAME_STATE_EXIT );
        break;

      case ALLEGRO_EVENT_KEY_DOWN:
        switch( event.keyboard.keycode )
          {
          case ALLEGRO_KEY_M:
          case ALLEGRO_KEY_ESCAPE:
            gam_set_state( gam, GAME_STATE_MENU );
            break;

          case ALLEGRO_KEY_A:
            fl_resize_windnow( alg, cfg, res, new_scale );
            scale       = cfg_get_scale( cfg );
            new_scale   = scale;
            event_queue = alg_get_event_queue( alg );
            al_flush_event_queue( event_queue );
            break;

          case ALLEGRO_KEY_LEFT:
          case ALLEGRO_KEY_PAD_MINUS:
            if( new_scale > CFG_SCALE_1 )
              new_scale--;
            break;

          case ALLEGRO_KEY_RIGHT:
          case ALLEGRO_KEY_PAD_PLUS:
            if( new_scale < CFG_SCALE_3 )
              new_scale++;
            break;

          case ALLEGRO_KEY_R:
            fl_reset_progress( alg, gam );
            al_flush_event_queue( event_queue );
            break;
          }
        break;

      case ALLEGRO_EVENT_TIMER:
        draw = true;
        break;

      case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
        if( event.mouse.button == MOUSE_BUTTON_1 )
          {
          int mx, my;
          int eid;

          mx = event.mouse.x / scale;
          my = event.mouse.y / scale;

          eid = uin_check_mouse_click( &uin, mx, my );
          switch( eid )
            {
            case UIE_MS_BUTTON_SCALE_LEFT:
              if( new_scale > CFG_SCALE_1 )
                new_scale--;
              break;

            case UIE_MS_BUTTON_SCALE_RIGHT:
              if( new_scale < CFG_SCALE_3 )
                new_scale++;
              break;

            case UIE_MS_BUTTON_RESET_PROGRESS:
              fl_reset_progress( alg, gam );
              al_flush_event_queue( event_queue );
              break;

            case UIE_MS_BUTTON_APPLY:
              fl_resize_windnow( alg, cfg, res, new_scale );
              scale       = cfg_get_scale( cfg );
              new_scale   = scale;
              event_queue = alg_get_event_queue( alg );
              al_flush_event_queue( event_queue );
              break;

            case UIE_MS_BUTTON_MENU:
              gam_set_state( gam, GAME_STATE_MENU );
              break;

            default:
              break;
            }
          }
        break;
      }

    if( draw && al_is_event_queue_empty( event_queue ) )
      {
      draw = false;

      al_clear_to_color     ( al_map_rgb( 0, 0, 0 ) );
      al_hold_bitmap_drawing( true );
      draw_menu_settings    ( cfg, gam, res, new_scale );
      draw_ui               ( res, &uin );
      al_hold_bitmap_drawing( false );

      al_flip_display();
      }
    }

  return 0;
  }

int game_menu_help( struct s_allegro   *alg,
                    struct s_config    *cfg,
                    struct s_game      *gam,
                    struct s_resources *res )
  {
  ALLEGRO_EVENT_QUEUE     *event_queue;
  ALLEGRO_EVENT            event;
  struct s_user_interface  uin;
  bool                     draw;
  int                      scale;
  int                      help_page;

  assert( alg );
  assert( gam );
  assert( res );

  uin_clear          ( &uin );
  uin_build_menu_help( &uin );

  event_queue = alg_get_event_queue( alg );
  scale       = cfg_get_scale( cfg );
  draw        = false;
  help_page   = 0;

  al_flush_event_queue( event_queue );

  while( gam_get_state( gam ) == GAME_STATE_MENU_HELP )
    {
    al_wait_for_event( event_queue, &event );

    switch( event.type )
      {
      case ALLEGRO_EVENT_DISPLAY_CLOSE:
        gam_set_state( gam, GAME_STATE_EXIT );
        break;

      case ALLEGRO_EVENT_KEY_DOWN:
        switch ( event.keyboard.keycode )
          {
          case ALLEGRO_KEY_M:
          case ALLEGRO_KEY_ESCAPE:
            gam_set_state( gam, GAME_STATE_MENU );
            break;

          case ALLEGRO_KEY_LEFT:
          case ALLEGRO_KEY_PAD_MINUS:
            help_page--;
            if( help_page < 0 )
              help_page = 0;
            break;

          case ALLEGRO_KEY_RIGHT:
          case ALLEGRO_KEY_PAD_PLUS:
            help_page++;
            if( help_page > 3 )
              help_page = 3;
            break;
          }
        break;

      case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
        if( event.mouse.button == MOUSE_BUTTON_1 )
          {
          int mx;
          int my;
          int eid;

          mx = event.mouse.x / scale;
          my = event.mouse.y / scale;

          eid = uin_check_mouse_click( &uin, mx, my );
          switch( eid )
            {
            case UIE_MH_BUTTON_MENU:
              gam_set_state( gam, GAME_STATE_MENU );
              break;

            case UIE_MH_BUTTON_PAGE_MINUS:
              help_page--;
              if( help_page < 0 )
                help_page = 0;
              break;

            case UIE_MH_BUTTON_PAGE_PLUS:
              help_page++;
              if( help_page > 3 )
                help_page = 3;
              break;
            }
          }
        break;

      case ALLEGRO_EVENT_TIMER:
        draw = true;
        break;
      }

    if( draw && al_is_event_queue_empty( event_queue ) )
      {
      draw = false;

      al_clear_to_color( al_map_rgb( 0, 0, 0 ) );

      al_hold_bitmap_drawing( true );

      draw_menu_help( res, help_page );
      draw_ui( res, &uin );

      al_hold_bitmap_drawing( false );

      al_flip_display();
      }
    }

  return 0;
  }
