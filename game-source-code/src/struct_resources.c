#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include "allegro5/allegro.h"
#include "allegro5/allegro_font.h"

#include "logfile.h"
#include "sprites.h"
#include "struct_resources.h"

struct s_sprite_res_data
  {
  int x;
  int y;
  int w;
  int h;
  };

// srd = Sprite Resource Data
const struct s_sprite_res_data srd[ SPR_COUNT ] =
  {
//{   x,   y,   w,   h },

  // Button PLAY:
  { 320, 128,  96,  32 },
  // Button CUSTOM:
  { 320, 160,  96,  32 },
  // Button EDITOR:
  { 320, 192,  96,  32 },
  // Button HELP:
  { 320, 224,  96,  32 },
  // Button SETTINGS:
  { 320, 256,  96,  32 },
  // Button EXIT:
  { 416, 128,  96,  32 },

  // Button CLEAR:
  { 384,  64,  64,  32 },
  // Button LOAD:
  { 448,  64,  64,  32 },
  // Button SAVE:
  { 384,  96,  64,  32 },
  // Button MENU:
  { 448,  96,  64,  32 },

  // Button APPLY:
  { 448, 160,  64,  32 },
  // Button RESET:
  { 448, 192,  64,  32 },

  // Button LEFT:
  { 448, 224,  32,  32 },
  // Button RIGHT:
  { 448, 256,  32,  32 },

  // Button RESET PROGRESS:
  { 320, 288, 160,  32 },

  // Button NEXT LEVEL:
  { 320, 320, 128,  32 },

  // Button UNDO:
  { 448, 384,  64,  32 },

  // Button SHIFT UP:
  { 416, 160,  32,  32 },
  // Button SHIFT RIGHT:
  { 416, 192,  32,  32 },
  // Button SHIFT DOWN:
  { 416, 224,  32,  32 },
  // Button SHIFT LEFT:
  { 416, 256,  32,  32 },

  // Brush WALL:
  { 384, 480,  32,  32 },
  // Brush DESTINATION:
  { 416, 480,  32,  32 },
  // Brush CRATE:
  { 448, 480,  32,  32 },
  // Brush START:
  { 480, 480,  32,  32 },

  // Brush selector:
  {   0, 256,  32,  32 },

  // Empty tile:
  { 480, 224,  32,  32 },
  // Crate:
  { 480, 256,  32,  32 },
  // Destination:
  { 480, 288,  32,  32 },
  // Marker:
  {   0,  96,  32,  32 },

  // Level satus COMPLETED:
  { 320,   0,  64,  64 },
  // Level satus AVIABLE:
  { 384,   0,  64,  64 },
  // Level satus LOCKED:
  { 448,   0,  64,  64 },

  // Level selection BACKGROUND:
  { 320,  64,  64,  64 },

  // Crate count background:
  {   0, 128,  64,  32 },
  // Destination count background:
  {   0, 288,  64,  32 },

  // Player UP:
  { 256, 480,  32,  32 },
  // Player RIGHT:
  { 288, 480,  32,  32 },
  // Player DOWN:
  { 320, 480,  32,  32 },
  // Player LEFT:
  { 352, 480,  32,  32 },

  // Title bitmap:
  {   0, 320, 320, 160 },

  // Popup window corner up left:
  { 448, 320,  32,  32 },
  // Popup window corner up right:
  { 480, 320,  32,  32 },
  // Popup window corner down left:
  { 448, 352,  32,  32 },
  // Popup window corner down rigth:
  { 480, 352,  32,  32 },

  // Popup window left:
  { 320, 352,  32,  32 },
  // Popup window right:
  { 352, 352,  32,  32 },
  // Popup window up:
  { 384, 352,  32,  32 },
  // Popup window down:
  { 416, 352,  32,  32 },
  // Popup window background:
  { 320, 384,  32,  32 },

  // Level text background right:
  { 352, 384,  32,  32 },
  // Level text background midle:
  { 384, 384,  32,  32 },
  // Level text background left:
  { 416, 384,  32,  32 },

  // Wall blob ( 47 tiles ):
  {   0 + 160,   0 +  32,  32,  32 },
  {   0 +   0,   0 +  64,  32,  32 },
  {   0 +  32,   0 +  96,  32,  32 },
  {   0 + 128,   0 +  64,  32,  32 },
  {   0 +  32,   0 +  64,  32,  32 },
  {   0 +   0,   0 +   0,  32,  32 },
  {   0 +   0,   0 +  32,  32,  32 },
  {   0 + 128,   0 +   0,  32,  32 },
  {   0 + 128,   0 +  32,  32,  32 },
  {   0 + 192,   0 +  96,  32,  32 },
  {   0 +  32,   0 +   0,  32,  32 },
  {   0 + 128,   0 + 128,  32,  32 },
  {   0 +  32,   0 +  32,  32,  32 },
  {   0 +  96,   0 +  96,  32,  32 },
  {   0 + 192,   0 +  64,  32,  32 },
  {   0 +  64,   0 +  96,  32,  32 },
  {   0 + 160,   0 +  64,  32,  32 },
  {   0 + 160,   0 + 128,  32,  32 },
  {   0 + 192,   0 +   0,  32,  32 },
  {   0 + 192,   0 +  32,  32,  32 },
  {   0 + 160,   0 +   0,  32,  32 },
  {   0 + 256,   0 +  32,  32,  32 },
  {   0 + 288,   0 +  96,  32,  32 },
  {   0 + 224,   0 +  96,  32,  32 },
  {   0 + 288,   0 + 128,  32,  32 },
  {   0 + 288,   0 +  32,  32,  32 },
  {   0 +  96,   0 +   0,  32,  32 },
  {   0 + 224,   0 + 128,  32,  32 },
  {   0 + 128,   0 +  96,  32,  32 },
  {   0 + 256,   0 + 128,  32,  32 },
  {   0 +  96,   0 + 128,  32,  32 },
  {   0 +  64,   0 +   0,  32,  32 },
  {   0 + 256,   0 +  64,  32,  32 },
  {   0 + 288,   0 +  64,  32,  32 },
  {   0 +  96,   0 +  64,  32,  32 },
  {   0 + 192,   0 + 128,  32,  32 },
  {   0 +  64,   0 +  64,  32,  32 },
  {   0 + 160,   0 +  96,  32,  32 },
  {   0 + 256,   0 +  96,  32,  32 },
  {   0 + 256,   0 +   0,  32,  32 },
  {   0 +  64,   0 + 128,  32,  32 },
  {   0 + 288,   0 +   0,  32,  32 },
  {   0 +  96,   0 +  32,  32,  32 },
  {   0 + 224,   0 +  32,  32,  32 },
  {   0 + 224,   0 +   0,  32,  32 },
  {   0 + 224,   0 +  64,  32,  32 },
  {   0 +  64,   0 +  32,  32,  32 },

  // Floor blob ( 47 tiles ):
  {   0 + 160, 160 +  32,  32,  32 },
  {   0 +   0, 160 +  64,  32,  32 },
  {   0 +  32, 160 +  96,  32,  32 },
  {   0 + 128, 160 +  64,  32,  32 },
  {   0 +  32, 160 +  64,  32,  32 },
  {   0 +   0, 160 +   0,  32,  32 },
  {   0 +   0, 160 +  32,  32,  32 },
  {   0 + 128, 160 +   0,  32,  32 },
  {   0 + 128, 160 +  32,  32,  32 },
  {   0 + 192, 160 +  96,  32,  32 },
  {   0 +  32, 160 +   0,  32,  32 },
  {   0 + 128, 160 + 128,  32,  32 },
  {   0 +  32, 160 +  32,  32,  32 },
  {   0 +  96, 160 +  96,  32,  32 },
  {   0 + 192, 160 +  64,  32,  32 },
  {   0 +  64, 160 +  96,  32,  32 },
  {   0 + 160, 160 +  64,  32,  32 },
  {   0 + 160, 160 + 128,  32,  32 },
  {   0 + 192, 160 +   0,  32,  32 },
  {   0 + 192, 160 +  32,  32,  32 },
  {   0 + 160, 160 +   0,  32,  32 },
  {   0 + 256, 160 +  32,  32,  32 },
  {   0 + 288, 160 +  96,  32,  32 },
  {   0 + 224, 160 +  96,  32,  32 },
  {   0 + 288, 160 + 128,  32,  32 },
  {   0 + 288, 160 +  32,  32,  32 },
  {   0 +  96, 160 +   0,  32,  32 },
  {   0 + 224, 160 + 128,  32,  32 },
  {   0 + 128, 160 +  96,  32,  32 },
  {   0 + 256, 160 + 128,  32,  32 },
  {   0 +  96, 160 + 128,  32,  32 },
  {   0 +  64, 160 +   0,  32,  32 },
  {   0 + 256, 160 +  64,  32,  32 },
  {   0 + 288, 160 +  64,  32,  32 },
  {   0 +  96, 160 +  64,  32,  32 },
  {   0 + 192, 160 + 128,  32,  32 },
  {   0 +  64, 160 +  64,  32,  32 },
  {   0 + 160, 160 +  96,  32,  32 },
  {   0 + 256, 160 +  96,  32,  32 },
  {   0 + 256, 160 +   0,  32,  32 },
  {   0 +  64, 160 + 128,  32,  32 },
  {   0 + 288, 160 +   0,  32,  32 },
  {   0 +  96, 160 +  32,  32,  32 },
  {   0 + 224, 160 +  32,  32,  32 },
  {   0 + 224, 160 +   0,  32,  32 },
  {   0 + 224, 160 +  64,  32,  32 },
  {   0 +  64, 160 +  32,  32,  32 },

  // Floor decorations ( 8 tiles ):
  {   0 +   0, 480,  32,  32  },
  {   0 +  32, 480,  32,  32  },
  {   0 +  64, 480,  32,  32  },
  {   0 +  96, 480,  32,  32  },
  {   0 + 128, 480,  32,  32  },
  {   0 + 160, 480,  32,  32  },
  {   0 + 192, 480,  32,  32  },
  {   0 + 224, 480,  32,  32  },

  };

static int fl_load_font( struct s_resources *res );

static int fl_load_font( struct s_resources *res )
  {
  ALLEGRO_BITMAP *bmp;
  int             ranges[] = { 32, 127 };

  assert( res );

  bmp = NULL;
  bmp = al_load_bitmap( "data/font-16x16.bmp" );
  if( !bmp )
    {
    logfile_write( "RES: Failed to load font bitmap." );
    logfile_write( "     FILE = data/font-16x16.bmp" );
/*
    printf( "RES: Failed to load font bitmap!\n" );
*/
    return -1;
    }

  al_convert_mask_to_alpha( bmp, al_map_rgb( 255, 0, 255 ) );

  res->font = al_grab_font_from_bitmap( bmp, 1, ranges );

  al_destroy_bitmap( bmp );

  if( !res->font )
    {
    logfile_write( "RES: Failed to create font." );
/*
    printf( "RES: Failed to create font!\n" );
*/
    return -1;
    }

  return 0;
  }

// Create resources structure:
struct s_resources *res_create_resources_struct()
  {
  struct s_resources *res;
  int i;

  res = NULL;
  res = malloc( sizeof( struct s_resources ) );
  if( !res )
    {
    logfile_write( "RES: Failed to allocate memory for resources structure." );
/*
    printf( "RES: Failed to allocate memory for resources structure!\n" );
*/
    return NULL;
    }

  // Default values:
  res->font         = NULL;
  res->sprite_sheet = NULL;

  for( i=0; i<SPR_COUNT; i++ )
    res->sprite[ i ] = NULL;

  res->initialized = false;

  return res;
  }

// Destroy resources structure:
void res_destroy_resources_struct( struct s_resources *res )
  {
  assert( res );

  res_destroy_all_internal_data( res );
  free( res );
  }

void res_destroy_all_internal_data( struct s_resources *res )
  {
  int i;

  assert( res );

  if( res->font )
    {
    al_destroy_font( res->font );
    res->font = NULL;
    }

  for( i=0; i<SPR_COUNT; i++ )
    if( res->sprite[ i ] )
      {
      al_destroy_bitmap( res->sprite[ i ] );
      res->sprite[ i ] = NULL;
      }

  if( res->sprite_sheet )
    {
    al_destroy_bitmap( res->sprite_sheet );
    res->sprite_sheet = NULL;
    }

  res->initialized = false;
  }

// Initialize resources struct:
int res_initialize( struct s_resources *res )
  {
  int i;

  assert( res );
  assert( res->initialized == false );

  res->sprite_sheet = al_create_bitmap( RES_SPRITE_SHEET_WIDTH,
                                        RES_SPRITE_SHEET_HEIGHT );
  if( !res->sprite_sheet )
    {
    logfile_write( "RES: Failed to create sprite sheet bitmap." );
/*
    printf( "RES: Failed to create sprite sheet bitmap!\n" );
*/
    return -1;
    }

  for( i=0; i<SPR_COUNT; i++ )
    {
    res->sprite[ i ] = al_create_sub_bitmap( res->sprite_sheet,
                                             srd[ i ].x,
                                             srd[ i ].y,
                                             srd[ i ].w,
                                             srd[ i ].h );
    if( !res->sprite[ i ] )
      {
      char str[ 256 ];

      sprintf( str, "RES: Failed to create sprite sub-bitmap id %d.", i );

      logfile_write( str );
/*
      printf( "RES: Failed to create sprite sub-bitmap id %d\n", i );
*/
      return -1;
      }
    }

  res->initialized = true;

  return 0;
  }

// Load all resources:
int res_load_resources ( struct s_resources *res )
  {
  assert( res );
  assert( res->initialized == true );

  ALLEGRO_BITMAP *bmp;
  ALLEGRO_STATE state;

  bmp = NULL;
  bmp = al_load_bitmap( "data/graphics.bmp" );
  if( !bmp )
    {
    logfile_write( "RES: Failed to load sprite sheet bitmap." );
    logfile_write( "     FILE = data/graphics.bmp" );
/*
    printf( "RES: Failed to load sprite sheet bitmap!\n" );
*/
    return -1;
    }

  al_store_state      ( &state, ALLEGRO_STATE_TARGET_BITMAP );
  al_set_target_bitmap( res->sprite_sheet );
  al_draw_bitmap      ( bmp, 0, 0, 0 );
  al_restore_state    ( &state );

  al_destroy_bitmap( bmp );

  al_convert_mask_to_alpha( res->sprite_sheet, al_map_rgb( 255, 0, 255) );

  if( fl_load_font( res ) == -1 )
    return -1;

  return 0;
  }

ALLEGRO_BITMAP *res_get_sprite( struct s_resources *res, int sprite_id )
  {
  assert( res );
  assert( sprite_id >= 0 && sprite_id < SPR_COUNT );

  return res->sprite[ sprite_id ];
  }
