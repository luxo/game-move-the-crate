#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "struct_player.h"

/*
struct s_player *plr_create_struct()
  {
  struct s_player *plr;

  plr = NULL;
  plr = malloc( sizeof( struct s_player ) );
  if( !plr )
    {
    printf( "PLR: Failed to create player struct!\n" );
    return NULL;
    }

  // Default values:
  plr->x = 0;
  plr->y = 0;

  return plr;
  }

void plr_destroy_struct( struct s_player *plr )
  {
  assert( plr );

  free( plr );
  }
*/

void plr_move( struct s_player *plr, int xd, int yd )
  {
  assert( plr );

  plr->x += xd;
  plr->y += yd;
  }

void plr_set_x( struct s_player *plr, int x )
  {
  assert( plr );

  plr->x = x;
  }

void plr_set_y( struct s_player *plr, int y )
  {
  assert( plr );

  plr->y = y;
  }

void plr_set_direction( struct s_player *plr, int direction )
  {
  assert( plr );

  plr->direction = direction;
  }

int plr_get_x( struct s_player *plr )
  {
  assert( plr );

  return plr->x;
  }

int plr_get_y( struct s_player *plr )
  {
  assert( plr );

  return plr->y;
  }

int plr_get_direction( struct s_player *plr )
  {
  assert( plr );

  return plr->direction;
  }
