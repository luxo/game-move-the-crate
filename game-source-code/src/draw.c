#include <assert.h>
#include <stdio.h>

#include "allegro5/allegro.h"

#include "draw.h"
#include "sprites.h"
#include "struct_config.h"
#include "struct_game.h"
#include "struct_map.h"
#include "struct_player.h"
#include "struct_resources.h"
#include "struct_user_interface.h"

const char level_status_text[ 3 ][ 10 ] =
  {
  "COMPLETED",
  "AVAILABLE",
  "LOCKED",
  };

void draw_player( struct s_player *plr, struct s_resources *res )
  {
  assert( plr );
  assert( res );

  al_draw_bitmap( res->sprite[ SPR_PLAYER_UP + plr->direction ],
                  plr->x * RES_SPRITE_WIDTH,
                  plr->y * RES_SPRITE_HEIGHT,
                  0 );
  }

// Draw map:
void draw_map( struct s_map *map,
               struct s_resources *res,
               bool draw_crates,                 // Crates
               bool draw_destinations,           // Destinations
               bool draw_player_start_position ) // Player start position
  {
  int i, j;

  assert( res );

  // Draw map terrain:
  for( i=0; i<MAP_WIDTH; i++ )
    for( j=0; j<MAP_HEIGHT; j++ )
      {
      switch( map->tile[ i ][ j ] )
        {
        case MAP_TILE_FLOOR:
          al_draw_bitmap( res->sprite[ SPR_FLOOR_BLOB_START + map->blob_id[ i ][ j ] ],
                          i * RES_SPRITE_WIDTH,
                          j * RES_SPRITE_HEIGHT,
                          0 );
          if( map->decoration[ i ][ j ] > 0 )
            al_draw_bitmap( res->sprite[ SPR_FLOOR_DECORATION_START + map->decoration[ i ][ j ] - 1 ],
                            i * RES_SPRITE_WIDTH,
                            j * RES_SPRITE_HEIGHT,
                            0 );
          break;

        case MAP_TILE_WALL:
          al_draw_bitmap( res->sprite[ SPR_WALL_BLOB_START + map->blob_id[ i ][ j ] ],
                          i * RES_SPRITE_WIDTH,
                          j * RES_SPRITE_HEIGHT,
                          0 );
          break;

        case MAP_TILE_EMPTY:
          al_draw_bitmap( res->sprite[ SPR_EMPTY_TILE ],
                          i * RES_SPRITE_WIDTH,
                          j * RES_SPRITE_HEIGHT,
                          0 );
          break;

        default:
          printf( "DRAW ERROR: Unknown map tile! VALUE = %d\n", map->tile[ i ][ j ] );
          exit( -1 );
          break;
        }
      }

  // Draw destinations:
  if( draw_destinations )
    for( i=0; i<map->destination_count; i++ )
      al_draw_bitmap( res->sprite[ SPR_DESTINATION ],
                      map->destination[ i ].x * RES_SPRITE_WIDTH,
                      map->destination[ i ].y * RES_SPRITE_HEIGHT,
                      0 );

  // Draw crates:
  if( draw_crates )
    for( i=0; i<map->crate_count; i++ )
      al_draw_bitmap( res->sprite[ SPR_CRATE ],
                      map->crate[ i ].x * RES_SPRITE_WIDTH,
                      map->crate[ i ].y * RES_SPRITE_HEIGHT,
                      0 );

  // Draw player start position:
  if( draw_player_start_position )
    al_draw_bitmap( res->sprite[ SPR_PLAYER_UP + map->start_direction ],
                    map->start_x_pos * RES_SPRITE_WIDTH,
                    map->start_y_pos * RES_SPRITE_HEIGHT,
                    0 );
  }

void draw_marker( struct s_resources *res, int x, int y )
  {
  assert( res );

  al_draw_bitmap( res->sprite[ SPR_MARKER ],
                  x * RES_SPRITE_WIDTH,
                  y * RES_SPRITE_HEIGHT,
                  0 );
  }


void draw_brush_selector( struct s_resources *res, int brush )
  {
  assert( res );

  al_draw_bitmap( res->sprite[ SPR_BRUSH_SELECTOR ],
                  brush * RES_SPRITE_WIDTH,
                  0,
                  0 );
  }

void draw_c_d_counts( struct s_map *map, struct s_resources *res )
  {
  assert( map );
  assert( res );

  al_draw_textf( res->font,
                 al_map_rgb( 0, 255, 0 ),
                 32,
                 456,
                 0,
                 "%2d",
                 map->crate_count );

  al_draw_textf( res->font,
                 al_map_rgb( 0, 255, 0 ),
                 96,
                 456,
                 0,
                 "%2d",
                 map->destination_count );
  }

void draw_menu_level_selection( struct s_game *gam, struct s_resources *res )
  {
  ALLEGRO_COLOR color[ 3 ];
  int           i, j;
  char          buff[ 32 ];
  int           tw; // Text width
  int           lid; // Level ID

  assert( gam );
  assert( res );

  color[ 0 ] = al_map_rgb( 106, 158,  54 ); // Completed
  color[ 1 ] = al_map_rgb( 210, 125,  44 ); // Aviable
  color[ 2 ] = al_map_rgb( 188,  30,  30 ); // Locked

  sprintf( buff, "PAGE %2d / %2d", gam->page + 1, GAME_PAGE_COUNT );
  tw = al_get_text_width( res->font, buff );
  al_draw_text( res->font,
                al_map_rgb( 255, 255, 255 ),
                160 + ( ( 320 - tw ) / 2 ),
                176 - 32,
                0,
                buff );

  j = gam->selected_level / 5;
  i = gam->selected_level - ( j * 5 );
  al_draw_bitmap( res->sprite[ SPR_LEVEL_SELECTION_BACKGROUND ],
                  160 + ( i * 64 ),
                  176 + ( j * 64 ),
                  0 );

  for( i=0; i<5; i++ )
    for( j=0; j<2; j++ )
      {
      lid = ( gam->page * GAME_LEVELS_PER_PAGE ) + ( i + ( j * 5 ) );

      al_draw_bitmap( res->sprite[ SPR_LEVEL_STATUS_COMPLETED + gam->level[ lid ].status ],
                      160 + ( i * 64 ),
                      176 + ( j * 64 ),
                      0 );

      sprintf( buff, "%d", lid + 1 );
      tw = al_get_text_width( res->font, buff );
      al_draw_text( res->font,
                     color[ gam->level[ lid ].status ],
                     160 + ( i * 64 ) + 8 + ( ( 48 - tw ) / 2 ),
                     176 + ( j * 64 ) + 48,
                     0,
                     buff );
      }


  lid = ( gam->page * GAME_LEVELS_PER_PAGE ) + gam->selected_level;
  sprintf( buff,
           "LEVEL %d %s",
           lid + 1,
           level_status_text[ gam->level[ lid ].status ] );

  tw = al_get_text_width( res->font, buff );

  al_draw_text( res->font,
                color[ gam->level[ lid ].status ],
                160 + ( ( 320 - tw ) / 2 ),
                176 + 144,
                0,
                buff );
  }

void draw_menu_settings( struct s_config    *cfg,
                         struct s_game      *gam,
                         struct s_resources *res,
                         int scale )
  {
  const char str_window_size[] = "WINDOW SIZE";

  const char str_window_size_choice[ 3 ][ 8 ] =
    {
    "NORMAL",
    "DOUBLE",
    "TRIPLE"
    };

  ALLEGRO_FONT  *font;
  ALLEGRO_COLOR col_white;

  int str_w; // String width in pixels
  int str_x; // String x position
  int str_y; // String y position
  int str_c; // String choice;

  assert( cfg );
  assert( gam );
  assert( res );

  font = res->font;
  col_white = al_map_rgb( 255, 255, 255 );

  str_w = al_get_text_width( font, str_window_size );
  str_x = ( 640 - str_w ) / 2;
  str_y = 32;

  al_draw_text( font, col_white, str_x, str_y, 0, str_window_size );

  // Scale is saved from 1 - 3, we need 0 - 2 for index:
  str_c = scale - 1;
  str_w = al_get_text_width( font, str_window_size_choice[ str_c ] );
  str_x = ( 640 - str_w ) / 2;
  str_y = 64;

  al_draw_textf( font, col_white, str_x, str_y, 0, "%s", str_window_size_choice[ str_c ] );
  }

void draw_menu_help( struct s_resources *res, int page )
  {
  ALLEGRO_COLOR gray;
  ALLEGRO_COLOR white;
  assert( res );

  gray  = al_map_rgb( 128, 128, 128 );
  white = al_map_rgb( 255, 255, 255 );

  al_draw_textf( res->font, white, 16, 16, 0,
                 "Help page:    %d / 4", page + 1 );

  switch( page )
    {
    case 0:
      al_draw_text( res->font, gray, 16, 64, 0,
                    "Gameplay:" );

      al_draw_text( res->font, gray, 32, 96, 0,
                    "The goal of the game is to get all");
      al_draw_text( res->font, gray, 32, 112, 0,
                    "crates to their destinations." );

      al_draw_text( res->font, gray, 32, 144, 0,
                    "Use arrow keys to move." );
      break;

    case 1:
      al_draw_text( res->font, gray, 16, 64, 0,
                    "Editor:" );

      al_draw_text( res->font, gray, 32, 96, 0,
                    "Left mouse button:" );
      al_draw_text( res->font, gray, 64, 112, 0,
                    "- Place selected brush." );

      al_draw_text( res->font, gray, 32, 144, 0,
                    "Right mouse button:" );
      al_draw_text( res->font, gray, 64, 160, 0,
                    "- Delete selected brush." );

      al_draw_text( res->font, gray, 32, 192, 0,
                    "Mouse wheel press:" );
      al_draw_text( res->font, gray, 64, 208, 0,
                    "- Place random floor decoration if" );
      al_draw_text( res->font, gray, 64, 224, 0,
                    "  no decoration is present, or" );
      al_draw_text( res->font, gray, 64, 240, 0,
                    "  deletes floor decoration." );


      al_draw_text( res->font, gray, 32, 272, 0,
                    "Mouse wheel rotate:" );
      al_draw_text( res->font, gray, 64, 288, 0,
                    "- Scroll floor decorations." );
      al_draw_text( res->font, gray, 64, 304, 0,
                    "- Rotate start position." );

      al_draw_text( res->font, gray, 32, 336, 0,
                    "Hold left or right mouse button with" );
      al_draw_text( res->font, gray, 32, 352, 0,
                    "wall brush to draw or delete" );
      al_draw_text( res->font, gray, 32, 368, 0,
                    "continuosly." );
      break;

    case 2:
      al_draw_text( res->font, gray, 16, 64, 0,
                    "Editor:" );

      al_draw_text( res->font, gray, 32, 96, 0,
                    "Keyboard controls:");

      al_draw_text( res->font, gray, 64, 128, 0,
                    "F1 - Wall brush" );
      al_draw_text( res->font, gray, 64, 144, 0,
                    "F2 - Destination brush" );
      al_draw_text( res->font, gray, 64, 160, 0,
                    "F3 - Crate brush" );
      al_draw_text( res->font, gray, 64, 176, 0,
                    "F4 - Start brush" );

      al_draw_text( res->font, gray, 64, 192, 0,
                    "C  - Clear map" );
      al_draw_text( res->font, gray, 64, 208, 0,
                    "L  - Load map" );
      al_draw_text( res->font, gray, 64, 224, 0,
                    "S  - Save map" );
      al_draw_text( res->font, gray, 64, 256, 0,
                    "E or ESC - Exit editor" );
      break;

    case 3:
      al_draw_text( res->font, gray, 16, 64, 0,
                    "General keyboard controls:" );

      al_draw_text( res->font, gray, 32, 96, 0,
                    "All underlined letters on buttons");
      al_draw_text( res->font, gray, 32, 112, 0,
                    "can be used as keyboard shortcuts.");

      al_draw_text( res->font, gray, 32, 144, 0,
                    "Levels can be selected by arrow keys.");

      al_draw_text( res->font, gray, 32, 176, 0,
                    "PAGE UP and PAGE DOWN can be used to");
      al_draw_text( res->font, gray, 32, 192, 0,
                    "change level selection page.");
      break;
    }
  }

void draw_ui( struct s_resources *res, struct s_user_interface *uin )
  {
  int i;

  assert( res );
  assert( uin );

  for( i=0; i<uin->element_count; i++ )
    if( uin->element[ i ].visible == UI_VISIBLE_YES )
      al_draw_bitmap( res->sprite[ uin->element[ i ].sid ],
                      uin->element[ i ].dx,
                      uin->element[ i ].dy,
                      0 );
  }

void draw_popup_window( struct s_resources *res )
  {
  int i, j;

  assert( res );

  al_draw_bitmap( res->sprite[ SPR_PW_CORNER_UL ], 160, 128, 0 );
  al_draw_bitmap( res->sprite[ SPR_PW_CORNER_UR ], 448, 128, 0 );
  al_draw_bitmap( res->sprite[ SPR_PW_CORNER_DL ], 160, 320, 0 );
  al_draw_bitmap( res->sprite[ SPR_PW_CORNER_DR ], 448, 320, 0 );

  for( i=0; i<8; i++ )
    {
    al_draw_bitmap( res->sprite[ SPR_PW_CORNER_U ],
                    192 + ( i * RES_SPRITE_WIDTH ),
                    128,
                    0 );
    al_draw_bitmap( res->sprite[ SPR_PW_CORNER_D ],
                    192 + ( i * RES_SPRITE_WIDTH ),
                    320,
                    0 );
    }

  for( j=0; j<5; j++ )
    {
    al_draw_bitmap( res->sprite[ SPR_PW_CORNER_L ],
                    160,
                    160 + ( j * RES_SPRITE_HEIGHT ),
                    0 );

    al_draw_bitmap( res->sprite[ SPR_PW_CORNER_R ],
                    448,
                    160 + ( j * RES_SPRITE_HEIGHT ),
                    0 );
    }

  for( i=0; i<8; i++ )
    for( j=0; j<5; j++ )
      al_draw_bitmap( res->sprite[ SPR_PW_BACKGROUND ],
                      192 + ( i * RES_SPRITE_WIDTH ),
                      160 + ( j * RES_SPRITE_HEIGHT ),
                      0 );




  ALLEGRO_COLOR color;
  char str_congratulations[] = "CONGRATULATIONS";
  char str_level[]           = "LEVEL";
  char str_completed[]       = "COMPLETED";
  int str_w; // String width in pixels
  int str_x; // String x position
  int str_y; // String y position

  color = al_map_rgb( 106, 158,  54 );

  str_w = al_get_text_width( res->font, str_congratulations );
  str_x = 192 + ( 256 - str_w ) / 2;
  str_y = 160;
  al_draw_text( res->font, color, str_x, str_y, 0, str_congratulations );

  str_w = al_get_text_width( res->font, str_level );
  str_x = 192 + ( 256 - str_w ) / 2;
  str_y = 192;
  al_draw_text( res->font, color, str_x, str_y, 0, str_level );

  str_w = al_get_text_width( res->font, str_completed );
  str_x = 192 + ( 256 - str_w ) / 2;
  str_y = 224;
  al_draw_text( res->font, color, str_x, str_y, 0, str_completed );
  }

void draw_level_number_text( struct s_game *gam, struct s_resources *res )
  {
  ALLEGRO_COLOR color;
  char          str[ 16 ];
  int           str_w;
  int           str_x;
  int           str_y;
  int           i;

  assert( gam );
  assert( res );

  if( gam->game_type == GAME_TYPE_PLAY )
    {
    int level;

    level = ( gam->page * GAME_LEVELS_PER_PAGE ) + gam->selected_level;
    sprintf( str, "LEVEL %d", level + 1 );

    if( gam->level[ level ].status == GAME_LEVEL_STATUS_COMPLETED )
      color = al_map_rgb( 106, 158,  54 ); // Completed
    else
      color = al_map_rgb( 210, 125,  44 ); // Aviable
    }
  else
    {
    sprintf( str, "CUSTOM LEVEL" );
    color = al_map_rgb( 255, 255, 255 );
    }

  str_w = al_get_text_width( res->font, str );
  str_x = ( 640 - str_w ) / 2;
  str_y = 4;

  al_draw_bitmap( res->sprite[ SPR_LEVEL_TEXT_BACKGROUND_R ], 192, 0, 0);
  al_draw_bitmap( res->sprite[ SPR_LEVEL_TEXT_BACKGROUND_L ], 416, 0, 0);

  for( i=0; i<6; i++ )
    al_draw_bitmap( res->sprite[ SPR_LEVEL_TEXT_BACKGROUND_M ],
                    224 + ( i * 32 ),
                    0,
                    0 );

  al_draw_textf( res->font, color, str_x, str_y, 0, "%s", str );
  }
