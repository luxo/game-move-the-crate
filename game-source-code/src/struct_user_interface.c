#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include "logfile.h"
#include "sprites.h"
#include "struct_user_interface.h"

struct s_user_interface *uin_create_struct()
  {
  struct s_user_interface *uin;

  uin = NULL;
  uin = malloc( sizeof( struct s_user_interface ) );
  if( !uin )
    {
    logfile_write( "UIN: Failed to allocate memory for user interface struct." );
/*
    printf( "UIN: Failed to allocate memory for s_ui struct!\n" );
*/
    return NULL;
    }

  // Default values:
  uin_clear( uin );

  return uin;
  }

void uin_destroy_struct( struct s_user_interface *uin )
  {
  assert( uin );

  free( uin );
  }

void uin_clear( struct s_user_interface *uin )
  {
  int i;

  assert( uin );

  for( i=0; i<UI_ELEMENT_MAX_COUNT; i++ )
    {
    uin->element[ i ].dx = 0;
    uin->element[ i ].dy = 0;
    uin->element[ i ].cx = 0;
    uin->element[ i ].cy = 0;
    uin->element[ i ].cw = 0;
    uin->element[ i ].ch = 0;
    uin->element[ i ].type = 0;
    uin->element[ i ].visible = 0;
    uin->element[ i ].eid = 0;
    uin->element[ i ].sid = 0;
    }

  uin->element_count = 0;
  }

int uin_add_element( struct s_user_interface *uin,
                     int dx,
                     int dy,
                     int cx,
                     int cy,
                     int cw,
                     int ch,
                     int type,
                     int visible,
                     int eid,
                     int sid )
  {
  assert( uin );

  // Cannot add more then UI_ELEMENT_MAX_COUNT elements:
  if( uin->element_count >= UI_ELEMENT_MAX_COUNT )
    return -1;

  // Element type can be only button or image for now:
  if( !( type == UI_TYPE_BUTTON || type == UI_TYPE_IMAGE ) )
    return -1;

  // Element visibility can only have two values:
  if( !( visible == UI_VISIBLE_NO || visible == UI_VISIBLE_YES ) )
    return -1;

  // Button cannot have eid of value UI_NONE:
  if( type == UI_TYPE_BUTTON && eid == UIE_NONE )
    return -1;

  uin->element[ uin->element_count ].dx = dx;
  uin->element[ uin->element_count ].dy = dy;
  uin->element[ uin->element_count ].cx = cx;
  uin->element[ uin->element_count ].cy = cy;
  uin->element[ uin->element_count ].cw = cw;
  uin->element[ uin->element_count ].ch = ch;
  uin->element[ uin->element_count ].type = type;
  uin->element[ uin->element_count ].visible = visible;
  uin->element[ uin->element_count ].eid = eid;
  uin->element[ uin->element_count ].sid = sid;

  uin->element_count++;

  return 0;
  }

int uin_check_mouse_click( struct s_user_interface *uin, int mx, int my )
  {
  int i;

  assert( uin );

  for( i=0; i<uin->element_count; i++ )
    if( uin->element[ i ].type == UI_TYPE_BUTTON )
      if( ( mx >= uin->element[ i ].cx &&
            mx <  uin->element[ i ].cx + uin->element[ i ].cw ) &&
          ( my >= uin->element[ i ].cy &&
            my <  uin->element[ i ].cy + uin->element[ i ].ch ) )
        return uin->element[ i ].eid;

  return UIE_NONE;
  }


// Test ui set build functions:
void uin_build_menu( struct s_user_interface *uin )
  {
  assert( uin );

  uin_add_element( uin, 160,   0,  -1,  -1,  -1,  -1, UI_TYPE_IMAGE , UI_VISIBLE_YES, UIE_NONE, SPR_TITLE );

  uin_add_element( uin, 176, 136, 176 + 4, 136 + 4,  32 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_M_BUTTON_PAGE_MINUS, SPR_BUTTON_LEFT  );
  uin_add_element( uin, 432, 136, 432 + 4, 136 + 4,  32 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_M_BUTTON_PAGE_PLUS , SPR_BUTTON_RIGHT );

  uin_add_element( uin, 176, 352, 176 + 4, 352 + 4,  96 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_M_BUTTON_PLAY    , SPR_BUTTON_PLAY     );
  uin_add_element( uin, 272, 352, 272 + 4, 352 + 4,  96 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_M_BUTTON_CUSTOM  , SPR_BUTTON_CUSTOM   );
  uin_add_element( uin, 368, 352, 368 + 4, 352 + 4,  96 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_M_BUTTON_EDITOR  , SPR_BUTTON_EDITOR   );

  uin_add_element( uin, 176, 384, 176 + 4, 384 + 4,  96 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_M_BUTTON_HELP    , SPR_BUTTON_HELP     );
  uin_add_element( uin, 272, 384, 272 + 4, 384 + 4,  96 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_M_BUTTON_CONFIG  , SPR_BUTTON_SETTINGS );
  uin_add_element( uin, 368, 384, 368 + 4, 384 + 4,  96 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_M_BUTTON_EXIT    , SPR_BUTTON_EXIT     );

  uin_add_element( uin,  -1,  -1, 160, 176,  64,  64, UI_TYPE_BUTTON, UI_VISIBLE_NO, UIE_M_BUTTON_LEVEL_0, SPR_NONE );
  uin_add_element( uin,  -1,  -1, 224, 176,  64,  64, UI_TYPE_BUTTON, UI_VISIBLE_NO, UIE_M_BUTTON_LEVEL_1, SPR_NONE );
  uin_add_element( uin,  -1,  -1, 288, 176,  64,  64, UI_TYPE_BUTTON, UI_VISIBLE_NO, UIE_M_BUTTON_LEVEL_2, SPR_NONE );
  uin_add_element( uin,  -1,  -1, 352, 176,  64,  64, UI_TYPE_BUTTON, UI_VISIBLE_NO, UIE_M_BUTTON_LEVEL_3, SPR_NONE );
  uin_add_element( uin,  -1,  -1, 416, 176,  64,  64, UI_TYPE_BUTTON, UI_VISIBLE_NO, UIE_M_BUTTON_LEVEL_4, SPR_NONE );
  uin_add_element( uin,  -1,  -1, 160, 240,  64,  64, UI_TYPE_BUTTON, UI_VISIBLE_NO, UIE_M_BUTTON_LEVEL_5, SPR_NONE );
  uin_add_element( uin,  -1,  -1, 224, 240,  64,  64, UI_TYPE_BUTTON, UI_VISIBLE_NO, UIE_M_BUTTON_LEVEL_6, SPR_NONE );
  uin_add_element( uin,  -1,  -1, 288, 240,  64,  64, UI_TYPE_BUTTON, UI_VISIBLE_NO, UIE_M_BUTTON_LEVEL_7, SPR_NONE );
  uin_add_element( uin,  -1,  -1, 352, 240,  64,  64, UI_TYPE_BUTTON, UI_VISIBLE_NO, UIE_M_BUTTON_LEVEL_8, SPR_NONE );
  uin_add_element( uin,  -1,  -1, 416, 240,  64,  64, UI_TYPE_BUTTON, UI_VISIBLE_NO, UIE_M_BUTTON_LEVEL_9, SPR_NONE );
  }

void uin_build_menu_settings( struct s_user_interface *uin )
  {
  assert( uin );

  uin_add_element( uin, 224,  56, 224 + 4,  56 + 4,  32 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_MS_BUTTON_SCALE_LEFT , SPR_BUTTON_LEFT  );
  uin_add_element( uin, 384,  56, 384 + 4,  56 + 4,  32 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_MS_BUTTON_SCALE_RIGHT, SPR_BUTTON_RIGHT );

  uin_add_element( uin, 240, 192, 240 + 4, 192 + 4,  64 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_MS_BUTTON_APPLY      , SPR_BUTTON_APPLY );
  uin_add_element( uin, 336, 192, 336 + 4, 192 + 4,  64 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_MS_BUTTON_MENU       , SPR_BUTTON_MENU  );

  uin_add_element( uin, 240, 128, 240 + 4, 128 + 4, 160 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_MS_BUTTON_RESET_PROGRESS, SPR_BUTTON_RESET_PROGRESS );
  }

void uin_build_menu_help( struct s_user_interface *uin )
  {
  assert( uin );

  uin_add_element( uin, 560,   8, 560 + 4,   8 + 4,  64 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_MH_BUTTON_MENU, SPR_BUTTON_MENU );

  uin_add_element( uin, 192,   8, 192 + 4,   8 + 4,  32 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_MH_BUTTON_PAGE_MINUS, SPR_BUTTON_LEFT  );
  uin_add_element( uin, 336,   8, 336 + 4,   8 + 4,  32 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_MH_BUTTON_PAGE_PLUS , SPR_BUTTON_RIGHT );
  }

void uin_build_editor_ui( struct s_user_interface *uin )
  {
  assert( uin );

  uin_add_element( uin,   0,  64,   0 + 4,  64 + 4,  32 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_E_BUTTON_SHIFT_UP         , SPR_BUTTON_SHIFT_UP    );
  uin_add_element( uin,   0, 128,   0 + 4, 128 + 4,  32 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_E_BUTTON_SHIFT_RIGHT      , SPR_BUTTON_SHIFT_RIGHT );
  uin_add_element( uin,   0,  96,   0 + 4,  96 + 4,  32 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_E_BUTTON_SHIFT_DOWN       , SPR_BUTTON_SHIFT_DOWN  );
  uin_add_element( uin,   0, 160,   0 + 4, 160 + 4,  32 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_E_BUTTON_SHIFT_LEFT       , SPR_BUTTON_SHIFT_LEFT  );
  uin_add_element( uin, 384,   0, 384 + 4,   0 + 4,  64 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_E_BUTTON_CLEAR            , SPR_BUTTON_CLEAR       );
  uin_add_element( uin, 448,   0, 448 + 4,   0 + 4,  64 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_E_BUTTON_LOAD             , SPR_BUTTON_LOAD        );
  uin_add_element( uin, 512,   0, 512 + 4,   0 + 4,  64 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_E_BUTTON_SAVE             , SPR_BUTTON_SAVE        );
  uin_add_element( uin, 576,   0, 576 + 4,   0 + 4,  64 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_E_BUTTON_MENU             , SPR_BUTTON_MENU        );
  uin_add_element( uin,   0,   0,   0 + 4,   0 + 4,  32 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_E_BUTTON_BRUSH_WALL       , SPR_BRUSH_WALL         );
  uin_add_element( uin,  32,   0,  32 + 4,   0 + 4,  32 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_E_BUTTON_BRUSH_DESTINATION, SPR_BRUSH_DESTINATION  );
  uin_add_element( uin,  64,   0,  64 + 4,   0 + 4,  32 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_E_BUTTON_BRUSH_CRATE      , SPR_BRUSH_CRATE        );
  uin_add_element( uin,  96,   0,  96 + 4,   0 + 4,  32 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_E_BUTTON_BRUSH_START      , SPR_BRUSH_START        );

  uin_add_element( uin,   0, 448,   0, 448,  64,  32, UI_TYPE_IMAGE , UI_VISIBLE_YES, UIE_NONE, SPR_CRATE_COUNT_BACKGROUND      );
  uin_add_element( uin,  64, 448,  64, 448,  64,  32, UI_TYPE_IMAGE , UI_VISIBLE_YES, UIE_NONE, SPR_DESTINATION_COUNT_BACGROUND );
  }

void uin_build_play_ui( struct s_user_interface *uin )
  {
  assert( uin );

  uin_add_element( uin, 576,   0, 576 + 4,   0 + 4,  64 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_P_BUTTON_MENU , SPR_BUTTON_MENU  );
  uin_add_element( uin, 512,   0, 512 + 4,   0 + 4,  64 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_P_BUTTON_RESET, SPR_BUTTON_RESET );
  uin_add_element( uin, 448,   0, 448 + 4,   0 + 4,  64 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_P_BUTTON_UNDO , SPR_BUTTON_UNDO  );
  }

void uin_build_pw_play( struct s_user_interface *uin )
  {
  assert( uin );

  uin_add_element( uin, 256, 256, 256 + 4, 256 + 4, 128 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_P_PW_BUTTON_NEXT_LEVEL, SPR_BUTTON_NEXT_LEVEL );
  uin_add_element( uin, 288, 288, 288 + 4, 288 + 4,  64 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_P_PW_BUTTON_MENU      , SPR_BUTTON_MENU       );
  }

void uin_build_pw_custom( struct s_user_interface *uin )
  {
  assert( uin );

  uin_add_element( uin, 288, 272, 288 + 4, 272 + 4,  64 - 8,  32 - 8, UI_TYPE_BUTTON, UI_VISIBLE_YES, UIE_P_PW_BUTTON_MENU, SPR_BUTTON_MENU );
  }
