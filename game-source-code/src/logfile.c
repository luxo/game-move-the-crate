#include <stdio.h>
#include <time.h>

#include "logfile.h"

void logfile_write( const char *str )
  {
  FILE      *f;
  time_t     rawtime;
  struct tm *timeinfo;
  char       str_time[ 64 ];

  time( &rawtime );

  timeinfo = localtime( &rawtime );

  sprintf( str_time,
           "%d-%02d-%02d %02d:%02d:%02d - ",
           timeinfo->tm_year + 1900,
           timeinfo->tm_mon,
           timeinfo->tm_mday,
           timeinfo->tm_hour,
           timeinfo->tm_min,
           timeinfo->tm_sec );

  printf( "%s\n", str_time );


  f = NULL;
  f = fopen( "errorlog.txt", "a" );
  if( !f )
    return;

  fprintf( f, "%s%s\n", str_time, str );

  fclose( f );
  }
