#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "allegro5/allegro.h"
#include "allegro5/allegro_native_dialog.h"

#include "draw.h"
#include "editor.h"
#include "struct_allegro.h"
#include "struct_config.h"
#include "struct_map.h"
#include "struct_game.h"
#include "struct_resources.h"
#include "struct_user_interface.h"

enum
  {
  EDITOR_BRUSH_WALL        = 0,
  EDITOR_BRUSH_DESTINATION = 1,
  EDITOR_BRUSH_CRATE       = 2,
  EDITOR_BRUSH_START       = 3,

  EDITOR_SELECT_FILE_OK                      =  0,
  EDITOR_SELECT_FILE_COULD_NOT_CREATE_DIALOG = -1,
  EDITOR_SELECT_FILE_CANCELED                = -2,

  EDITOR_DIALOG_LOAD = 0,
  EDITOR_DIALOG_SAVE = 1,

  EDITOR_MARKER_POS_OUT_OF_MAP = -1,

  MOUSE_BUTTON_NONE = -1,
  MOUSE_BUTTON_1    =  1,
  MOUSE_BUTTON_2    =  2,
  MOUSE_BUTTON_3    =  3,

  MSGBOX_YES_BUTTON = 1,
  };

static  int fl_select_file_dialog( struct s_allegro *alg, char *path, int dialog_type );
static void fl_save_map          ( struct s_allegro *alg, struct s_map *map );
static void fl_load_map          ( struct s_allegro *alg, struct s_map *map );
static void fl_clear_map         ( struct s_allegro *alg, struct s_map *map );
static void fl_return_to_menu    ( struct s_allegro *alg, struct s_game *gam );

static int fl_select_file_dialog( struct s_allegro *alg, char *path, int dialog_type )
  {
  ALLEGRO_DISPLAY     *display;
  ALLEGRO_FILECHOOSER *dialog;

  assert( alg );

  display = alg_get_display( alg );

  dialog = NULL;

  if( dialog_type == EDITOR_DIALOG_SAVE )
    dialog = al_create_native_file_dialog( "",
                                           "Save map to file:",
                                           "",
                                           ALLEGRO_FILECHOOSER_SAVE );
  else
    dialog = al_create_native_file_dialog( "",
                                           "Load map from file:",
                                           "",
                                           ALLEGRO_FILECHOOSER_FILE_MUST_EXIST );

  if( dialog )
    {
    al_show_native_file_dialog( display, dialog );

    if( al_get_native_file_dialog_count( dialog ) > 0 )
      strcpy( path, al_get_native_file_dialog_path( dialog, 0 ) );
    else
      return EDITOR_SELECT_FILE_CANCELED;

    al_destroy_native_file_dialog( dialog );
    }
  else
    return EDITOR_SELECT_FILE_COULD_NOT_CREATE_DIALOG;

  return EDITOR_SELECT_FILE_OK;
  }

static void fl_save_map( struct s_allegro *alg, struct s_map *map )
  {
  ALLEGRO_DISPLAY *display;
  char             file_path[ 1024 ];
  int              rval;

  assert( alg );
  assert( map );

  display = alg_get_display( alg );

  if( !map_has_start_position( map ) )
    {
    al_show_native_message_box( display,
                                "Save map",
                                "Could not save the map !",
                                "Map does not have a starting postion !",
                                NULL,
                                ALLEGRO_MESSAGEBOX_ERROR );
    return;
    }

  if( !map_has_crates( map ) )
    {
    al_show_native_message_box( display,
                                "Save map",
                                "Could not save the map !",
                                "Map does not have any crates !",
                                NULL,
                                ALLEGRO_MESSAGEBOX_ERROR );
    return;
    }

  if( !map_has_equal_amount_of_crates_and_destinations( map ) )
    {
    al_show_native_message_box( display,
                                "Save map",
                                "Could not save the map !",
                                "Map requires equal amount of crates and destinations !",
                                NULL,
                                ALLEGRO_MESSAGEBOX_ERROR );
    return;
    }

  rval = fl_select_file_dialog( alg, file_path, EDITOR_DIALOG_SAVE );

  // Do not handle rval == EDITOR_SELECT_FILE_CANCELED. Just do nothing.

  if( rval == EDITOR_SELECT_FILE_COULD_NOT_CREATE_DIALOG )
    al_show_native_message_box( display,
                                "Save map",
                                "Could not save the map !",
                                "Failed to create file selection dialog !",
                                NULL,
                                ALLEGRO_MESSAGEBOX_ERROR );

  if( rval == EDITOR_SELECT_FILE_OK )
    {
    rval = map_save_map_to_file( map, file_path );

    if( rval == MAP_SAVE_FAILED_TO_OPEN_FILE )
      al_show_native_message_box( display,
                                  "Save map",
                                  "Could not save the map !",
                                  "Selected file could not be opened for writing!",
                                  NULL,
                                  ALLEGRO_MESSAGEBOX_ERROR );
    }
  }

static void fl_load_map( struct s_allegro *alg, struct s_map *map )
  {
  ALLEGRO_DISPLAY *display;
  char             file_path[ 1024 ];
  int              rval;

  assert( alg );
  assert( map );

  display = alg_get_display( alg );

  rval = fl_select_file_dialog( alg, file_path, EDITOR_DIALOG_LOAD );

  // Do not handle rval == EDITOR_SELECT_FILE_CANCELED. Just do nothing.

  if( rval == EDITOR_SELECT_FILE_COULD_NOT_CREATE_DIALOG )
    al_show_native_message_box( display,
                                "Load map",
                                "Could not load the map !",
                                "Failed to create file selection dialog !",
                                NULL,
                                ALLEGRO_MESSAGEBOX_ERROR );

  if( rval == EDITOR_SELECT_FILE_OK )
    {
    rval = map_load_map_from_file( map, file_path );

    if( rval == MAP_LOAD_FAILED_TO_OPEN_FILE )
      al_show_native_message_box( display,
                                  "Load map",
                                  "Could not load the map !",
                                  "Selected file could not be opened !",
                                  NULL,
                                  ALLEGRO_MESSAGEBOX_ERROR );

    if( rval == MAP_LOAD_NOT_A_MAP_FILE )
      al_show_native_message_box( display,
                                  "Load map",
                                  "Could not load the map !",
                                  "Selected file is not a map file !",
                                  NULL,
                                  ALLEGRO_MESSAGEBOX_ERROR );
    }
  }

static void fl_clear_map( struct s_allegro *alg, struct s_map *map )
  {
  ALLEGRO_DISPLAY *display;
  int              rval;

  assert( alg );
  assert( map );

  display = alg_get_display( alg );

  rval = al_show_native_message_box( display,
                                     "Clear map",
                                     "Really clear the map ?",
                                     "If you press YES the map will be cleared.",
                                     NULL,
                                     ALLEGRO_MESSAGEBOX_YES_NO );

  if( rval == MSGBOX_YES_BUTTON )
    map_clear_map( map );
  }

static void fl_return_to_menu( struct s_allegro *alg, struct s_game *gam )
  {
  ALLEGRO_DISPLAY *display;
  int              rval;

  assert( alg );
  assert( gam );

  display = alg_get_display( alg );

  rval = al_show_native_message_box( display,
                                     "Return to menu",
                                     "Really return to menu?",
                                     "Unsaved changes will be lost.",
                                     NULL,
                                     ALLEGRO_MESSAGEBOX_YES_NO );

  if( rval == MSGBOX_YES_BUTTON )
    gam_set_state( gam, GAME_STATE_MENU );
  }

void game_editor( struct s_allegro   *alg,
                  struct s_config    *cfg,
                  struct s_game      *gam,
/*
                  struct s_map       *map,
*/
                  struct s_resources *res )
  {
  ALLEGRO_EVENT_QUEUE     *event_queue;
  ALLEGRO_EVENT            event;
  struct s_map             map;
  struct s_user_interface  uin;
  bool                     draw;
  int                      brush;
  int                      marker_x;
  int                      marker_y;
  int                      scale;
  int                      held_button;

  assert( alg );
  assert( cfg );
  assert( gam );
  assert( res );

  event_queue = alg_get_event_queue( alg );
  scale       = cfg_get_scale      ( cfg );
  brush       = EDITOR_BRUSH_WALL;
  marker_x    = EDITOR_MARKER_POS_OUT_OF_MAP;
  marker_y    = EDITOR_MARKER_POS_OUT_OF_MAP;
  held_button = MOUSE_BUTTON_NONE;
  draw        = false;

  // Start editor with empty map:
  map_clear_map( &map );

  // Build user interface:
  uin_clear          ( &uin );
  uin_build_editor_ui( &uin );

  while( gam->state == GAME_STATE_EDITOR )
    {
    al_wait_for_event( event_queue, &event );

    switch( event.type )
      {
      case ALLEGRO_EVENT_DISPLAY_CLOSE:
        gam_set_state( gam, GAME_STATE_EXIT );
        break;

      case ALLEGRO_EVENT_KEY_DOWN:
        switch( event.keyboard.keycode )
          {
          case ALLEGRO_KEY_M:
          case ALLEGRO_KEY_ESCAPE:
            fl_return_to_menu( alg, gam );
            al_clear_keyboard_state( NULL );
            al_flush_event_queue( event_queue );
            break;

          case ALLEGRO_KEY_C:
            fl_clear_map( alg, &map );
            al_clear_keyboard_state( NULL );
            al_flush_event_queue( event_queue );
            break;

          case ALLEGRO_KEY_L:
            fl_load_map( alg, &map );
            al_clear_keyboard_state( NULL );
            al_flush_event_queue( event_queue );
            break;

          case ALLEGRO_KEY_S:
            fl_save_map( alg, &map );
            al_clear_keyboard_state( NULL );
            al_flush_event_queue( event_queue );
            break;

          case ALLEGRO_KEY_F1:
            brush = EDITOR_BRUSH_WALL;
            break;

          case ALLEGRO_KEY_F2:
            brush = EDITOR_BRUSH_DESTINATION;
            break;

          case ALLEGRO_KEY_F3:
            brush = EDITOR_BRUSH_CRATE;
            break;

          case ALLEGRO_KEY_F4:
            brush = EDITOR_BRUSH_START;
            break;
          }
        break;

      case ALLEGRO_EVENT_MOUSE_AXES:
        marker_x = ( event.mouse.x / scale ) / RES_SPRITE_WIDTH;
        marker_y = ( event.mouse.y / scale ) / RES_SPRITE_HEIGHT;

        if( ( marker_x < 1 || marker_x > MAP_WIDTH - 2 ) ||
            ( marker_y < 1 || marker_y > MAP_HEIGHT - 2 ) )
          {
          marker_x = EDITOR_MARKER_POS_OUT_OF_MAP;
          marker_y = EDITOR_MARKER_POS_OUT_OF_MAP;
          }

        // Mouse wheel moved? If yes, scroll the decorations:
        if( event.mouse.dz != 0 )
          if( marker_x != EDITOR_MARKER_POS_OUT_OF_MAP )
            {
            if( map_is_start_position_on_coordinates( &map, marker_x, marker_y ) )
              map_editor_rotate_start_direction( &map,
                                                 marker_x,
                                                 marker_y,
                                                 event.mouse.dz );
            else
              map_editor_scroll_decoration( &map,
                                            marker_x,
                                            marker_y,
                                            event.mouse.dz );
            }

        // Continuous drawing:
        if( held_button == MOUSE_BUTTON_1 || held_button == MOUSE_BUTTON_2 )
          if( brush == EDITOR_BRUSH_WALL )
            if( marker_x != EDITOR_MARKER_POS_OUT_OF_MAP )
              {
              if( held_button == MOUSE_BUTTON_1 )
                map_editor_place_wall( &map, marker_x, marker_y );
              else
                map_editor_place_floor( &map, marker_x, marker_y );
              }
        break;

      case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
        // Left mouse button pressed
        if( event.mouse.button == MOUSE_BUTTON_1 )
          {
          int mx, my;
          int eid;

          mx = event.mouse.x / scale;
          my = event.mouse.y / scale;

          eid = uin_check_mouse_click( &uin, mx, my );
          switch( eid )
            {
            case UIE_E_BUTTON_SHIFT_UP:
              map_shift_up( &map );
              break;

            case UIE_E_BUTTON_SHIFT_RIGHT:
              map_shift_right( &map );
              break;

            case UIE_E_BUTTON_SHIFT_DOWN:
              map_shift_down( &map );
              break;

            case UIE_E_BUTTON_SHIFT_LEFT:
              map_shift_left( &map );
              break;

            case UIE_E_BUTTON_CLEAR:
              fl_clear_map( alg, &map );
              al_flush_event_queue( event_queue );
              break;

            case UIE_E_BUTTON_LOAD:
              fl_load_map( alg, &map );
              al_flush_event_queue( event_queue );
              break;

            case UIE_E_BUTTON_SAVE:
              fl_save_map( alg, &map );
              al_flush_event_queue( event_queue );
              break;

            case UIE_E_BUTTON_MENU:
              fl_return_to_menu( alg, gam );
              break;

            case UIE_E_BUTTON_BRUSH_WALL:
              brush = EDITOR_BRUSH_WALL;
              break;

            case UIE_E_BUTTON_BRUSH_DESTINATION:
              brush = EDITOR_BRUSH_DESTINATION;
              break;

            case UIE_E_BUTTON_BRUSH_CRATE:
              brush = EDITOR_BRUSH_CRATE;
              break;

            case UIE_E_BUTTON_BRUSH_START:
              brush = EDITOR_BRUSH_START;
              break;
            }

          if( marker_x != EDITOR_MARKER_POS_OUT_OF_MAP )
            {
            held_button = MOUSE_BUTTON_1;

            switch( brush )
              {
              case EDITOR_BRUSH_WALL:
                map_editor_place_wall( &map, marker_x, marker_y );
                break;

              case EDITOR_BRUSH_DESTINATION:
                map_editor_place_destination( &map, marker_x, marker_y );
                break;

              case EDITOR_BRUSH_CRATE:
                map_editor_place_crate( &map, marker_x, marker_y );
                break;

              case EDITOR_BRUSH_START:
                map_editor_place_start_position( &map, marker_x, marker_y );
                break;
              }
            }
          }

        // Right mouse button pressed:
        if( event.mouse.button == MOUSE_BUTTON_2 )
          {
          held_button = MOUSE_BUTTON_2;

          if( marker_x != EDITOR_MARKER_POS_OUT_OF_MAP )
            switch( brush )
              {
              case EDITOR_BRUSH_DESTINATION:
                map_editor_delete_destination( &map, marker_x, marker_y );
                break;

              case EDITOR_BRUSH_CRATE:
                map_editor_delete_crate( &map, marker_x, marker_y );
                break;

              case EDITOR_BRUSH_START:
                map_editor_delete_start_position( &map, marker_x, marker_y );
                break;

              case EDITOR_BRUSH_WALL:
                // RMB with wall brush deletes wall, therefore placing floor:
                map_editor_place_floor( &map, marker_x, marker_y );
                break;
              }
          }

        // Middle mouse button pressed:
        if( event.mouse.button == MOUSE_BUTTON_3 )
          if( marker_x != EDITOR_MARKER_POS_OUT_OF_MAP )
            map_editor_place_random_decoration( &map, marker_x, marker_y );
        break;

      case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
        held_button = MOUSE_BUTTON_NONE;
        break;

      case ALLEGRO_EVENT_TIMER:
        draw = true;
        break;
      }

    if( draw && al_is_event_queue_empty( event_queue ) )
      {
      draw = false;
      al_hold_bitmap_drawing( true );

      draw_map( &map, res, true, true, true );
      draw_ui( res, &uin );
      draw_brush_selector( res, brush );
      draw_c_d_counts( &map, res );

      // Marker:
      if( marker_x != EDITOR_MARKER_POS_OUT_OF_MAP )
        draw_marker( res, marker_x, marker_y );

      al_hold_bitmap_drawing( false );
      al_flip_display();
      }
    }
  }
