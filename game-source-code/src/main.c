#include "assert.h"
#include "stdbool.h"
#include "stdio.h"

#include "allegro5/allegro.h"

#include "editor.h"
#include "logfile.h"
#include "menu.h"
#include "play.h"
#include "struct_allegro.h"
#include "struct_config.h"
#include "struct_game.h"
#include "struct_resources.h"

int main( int argc, char *argv[] )
  {
  struct s_allegro   *alg;
  struct s_config    *cfg;
  struct s_game      *gam;
  struct s_resources *res;
  int                 scale;

  srand( time( NULL ) );

  cfg = NULL;
  cfg = cfg_create_struct();
  if( !cfg )
    return -1;
  if( cfg_load_from_file( cfg, "config.cfg" ) == -1 )
    {
    cfg_set_default_values( cfg );
    cfg_save_to_file( cfg, "config.cfg" );
    }
  scale = cfg_get_scale( cfg );

  alg = NULL;
  alg = alg_create_struct();
  if( alg == NULL )
    return -1;

  alg_set_display_flags     ( alg, ALLEGRO_WINDOWED | ALLEGRO_GENERATE_EXPOSE_EVENTS );
  alg_set_buffer_resolution ( alg, 640, 480 );
  alg_set_display_resolution( alg, 640 * scale, 480 * scale );
  alg_set_timer_speed       ( alg, 1.0 / 60.0 );

  if( alg_initialize( alg ) == -1 )
    return -1;

  alg_scale_buffer_to_display( alg );
  alg_set_window_title       ( alg, "Move the crate" );

  res = NULL;
  res = res_create_resources_struct();
  if( !res )
    return -2;
  if( res_initialize( res ) == -1 )
    return -3;
  if( res_load_resources( res ) == -1 )
    return -4;

  alg_set_display_icon( alg, res_get_sprite( res, SPR_CRATE ) );

  gam = NULL;
  gam = gam_create_game_struct();
  if( !gam )
    return -7;

  if( gam_load_progress( gam, "game.sav" ) != 0 )
    {
    gam_reset_progress( gam );
    gam_save_progress( gam, "game.sav" );
    }

  while( gam_get_state( gam ) != GAME_STATE_EXIT )
    {
    switch( gam_get_state( gam ) )
      {
      case GAME_STATE_MENU:
        game_menu( alg, cfg, gam, res );
        break;

      case GAME_STATE_MENU_SETTINGS:
        game_menu_settings( alg, cfg, gam, res );
        break;

      case GAME_STATE_MENU_HELP:
        game_menu_help( alg, cfg, gam, res );
        break;

      case GAME_STATE_PLAY:
        game_play( alg, cfg, gam, res );
        break;

      case GAME_STATE_EDITOR:
        game_editor( alg, cfg, gam, res );
        break;

      default:
        logfile_write( "MAIN: Unknown game state. Aborting." );
/*
        printf( "Unknown game state! Aborting!\n" );
*/
        gam_set_state( gam, GAME_STATE_EXIT );
        break;
      }
    }

  gam_destroy_game_struct( gam );
  gam = NULL;

  res_destroy_resources_struct( res );
  res = NULL;

  alg_destroy_struct( alg );
  alg = NULL;

  cfg_destroy_struct( cfg );
  cfg = NULL;

  return 0;
  }
