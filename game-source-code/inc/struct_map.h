#ifndef STRUCT_MAP_H_INCLUDED
#define STRUCT_MAP_H_INCLUDED

#include <stdbool.h>
#include <stdint.h>

enum
  {
  MAP_MAX_CRATES       = 32,
  MAP_MAX_DESTINATIONS = 32,
  MAP_MAX_DECORATIONS  = 8,

  MAP_WIDTH  = 20,
  MAP_HEIGHT = 15,

  MAP_TILE_WALL  = 0,
  MAP_TILE_FLOOR = 1,
  MAP_TILE_EMPTY = 2,

  MAP_TILE_INFO_CAN_WALK = 0,
  MAP_TILE_INFO_BLOCKED  = 1,
  MAP_TILE_INFO_CRATE    = 2,

  MAP_LOAD_OK                  =  0,
  MAP_LOAD_FAILED_TO_OPEN_FILE = -1,
  MAP_LOAD_NOT_A_MAP_FILE      = -2,

  MAP_SAVE_OK                  =  0,
  MAP_SAVE_FAILED_TO_OPEN_FILE = -1,
  };

struct s_map
  {
  int8_t tile      [ MAP_WIDTH ][ MAP_HEIGHT ];
  int8_t blob_id   [ MAP_WIDTH ][ MAP_HEIGHT ];
  int8_t decoration[ MAP_WIDTH ][ MAP_HEIGHT ];

  int8_t crate_count;
  struct s_crate
    {
    int8_t x;
    int8_t y;
    } crate[ MAP_MAX_CRATES ];

  int8_t destination_count;
  struct s_destination
    {
    int8_t x;
    int8_t y;
    } destination[ MAP_MAX_DESTINATIONS ];

  int8_t start_x_pos;
  int8_t start_y_pos;
  int8_t start_direction;
  };

// Functions operating on map struct:
struct s_map *map_create_map_struct ();
void          map_destroy_map_struct( struct s_map *map );

int map_get_tile_info( struct s_map *map, int x, int y );
int map_get_crate_id( struct s_map *map, int x, int y );
void map_move_crate( struct s_map *map, int crate_id, int x, int y );

// Save map to file:
int map_save_map_to_file( struct s_map *map, const char *file_path );

// Load map from file
int map_load_map_from_file( struct s_map *map, const char *file_path );

// Make map empty:
void map_clear_map( struct s_map *map );

// Editor stuff:
void map_editor_place_wall          ( struct s_map *map, int x, int y );
void map_editor_place_floor         ( struct s_map *map, int x, int y );
void map_editor_place_crate         ( struct s_map *map, int x, int y );
void map_editor_place_destination   ( struct s_map *map, int x, int y );
void map_editor_place_start_position( struct s_map *map, int x, int y );

void map_editor_delete_crate         ( struct s_map *map, int x, int y );
void map_editor_delete_destination   ( struct s_map *map, int x, int y );
void map_editor_delete_start_position( struct s_map *map, int x, int y );

void map_shift_up   ( struct s_map *map );
void map_shift_right( struct s_map *map );
void map_shift_down ( struct s_map *map );
void map_shift_left ( struct s_map *map );

void map_editor_place_random_decoration( struct s_map *map, int x, int y );
void map_editor_scroll_decoration      ( struct s_map *map, int x, int y, int z );
void map_editor_rotate_start_direction ( struct s_map *map, int x, int y, int z );

bool map_has_start_position                         ( struct s_map *map );
bool map_has_crates                                 ( struct s_map *map );
bool map_has_equal_amount_of_crates_and_destinations( struct s_map *map );
bool map_is_start_position_on_coordinates           ( struct s_map *map, int x, int y );

bool map_is_file_valid_map( const char *file_path );

// Returns true if all crates are on destinations, otherwise false:
bool map_level_completed( struct s_map *map );

int map_get_start_x_pos    ( struct s_map *map );
int map_get_start_y_pos    ( struct s_map *map );
int map_get_start_direction( struct s_map *map );

int map_get_crate_x_pos( struct s_map *map, int crate_id );
int map_get_crate_y_pos( struct s_map *map, int crate_id );
void map_set_crate_position( struct s_map *map,
                             int           crate_id,
                             int           crate_x,
                             int           crate_y );

#endif // STRUCT_MAP_H_INCLUDED
