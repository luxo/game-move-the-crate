#ifndef STRUCT_ALLEGRO_H_INCLUDED
#define STRUCT_ALLEGRO_H_INCLUDED

#include <stdbool.h>

#include "allegro5/allegro.h"


// Display resolutions:
// Minimum resolution: MicroVision ( 16 x 16 )
// Maximum resolution: HXGA        ( 4096 x 3072 )
enum
  {
  ALG_RESOLUTION_MIN_W = 16,
  ALG_RESOLUTION_MIN_H = 16,
  ALG_RESOLUTION_MAX_W = 4096,
  ALG_RESOLUTION_MAX_H = 3072
  };

// Main Allegro structure:
struct s_allegro
  {
  ALLEGRO_DISPLAY *display;
  int display_flags;
  int display_width;
  int display_height;

  int buffer_width;
  int buffer_height;

  ALLEGRO_EVENT_QUEUE *event_queue;

  ALLEGRO_TIMER *timer;
  double timer_speed;

  bool initialized;
  };

// Creation stuff:
struct s_allegro *alg_create_struct ();
void              alg_destroy_struct( struct s_allegro *alg );

void alg_destroy_all_internal_data( struct s_allegro *alg );

// Initialization stuff:
void alg_set_display_flags     ( struct s_allegro *alg, int flags );
void alg_set_display_resolution( struct s_allegro *alg, int w, int h );
void alg_set_buffer_resolution ( struct s_allegro *alg, int w, int h );
void alg_set_timer_speed       ( struct s_allegro *alg, double ts );
int  alg_initialize            ( struct s_allegro *alg );

// Scale screen to display and keep aspect ratio:
void alg_scale_buffer_to_display( struct s_allegro *alg );

// Manipulation stuff:
void alg_start_timer( struct s_allegro *alg );
void alg_stop_timer ( struct s_allegro *alg );

void alg_set_window_title( struct s_allegro *alg, const char *window_title );

void alg_set_display_icon( struct s_allegro *alg, ALLEGRO_BITMAP *bmp );

// Getters:
ALLEGRO_DISPLAY     *alg_get_display    ( struct s_allegro *alg );
ALLEGRO_EVENT_QUEUE *alg_get_event_queue( struct s_allegro *alg );


#endif // STRUCT_ALLEGRO_H_INCLUDED
