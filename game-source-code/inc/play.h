#ifndef PLAY_H_INCLUDED
#define PLAY_H_INCLUDED

struct s_allegro;
struct s_config;
struct s_game;
struct s_resources;

int game_play( struct s_allegro   *alg,
               struct s_config    *cfg,
               struct s_game      *gam,
               struct s_resources *res );

#endif // PLAY_H_INCLUDED
