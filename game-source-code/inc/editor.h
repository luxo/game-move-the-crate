#ifndef EDITOR_H_INCLUDED
#define EDITOR_H_INCLUDED

// Forward declarations:
struct s_allegro;
struct s_config;
struct s_game;
struct s_resources;

void game_editor( struct s_allegro   *frw,
                  struct s_config    *cfg,
                  struct s_game      *gam,
                  struct s_resources *res );

#endif // EDITOR_H_INCLUDED
