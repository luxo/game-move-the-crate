#ifndef STRUCT_PLAYER_H_INCLUDED
#define STRUCT_PLAYER_H_INCLUDED

struct s_player
  {
  int x;
  int y;
  int direction;
  };

/*
struct s_player *plr_create_struct();
void             plr_destroy_struct( struct s_player *plr );
*/

void plr_move( struct s_player *plr, int xd, int yd );

void plr_set_x        ( struct s_player *plr, int x );
void plr_set_y        ( struct s_player *plr, int y );
void plr_set_direction( struct s_player *plr, int direction );

int plr_get_x        ( struct s_player *plr );
int plr_get_y        ( struct s_player *plr );
int plr_get_direction( struct s_player *plr );

#endif // STRUCT_PLAYER_H_INCLUDED
