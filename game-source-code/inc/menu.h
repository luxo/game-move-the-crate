#ifndef MENU_H_INCLUDED
#define MENU_H_INCLUDED

struct s_allegro;
struct s_config;
struct s_game;
struct s_resources;

int game_menu( struct s_allegro   *alg,
               struct s_config    *cfg,
               struct s_game      *gam,
               struct s_resources *res );

int game_menu_settings( struct s_allegro   *alg,
                        struct s_config    *cfg,
                        struct s_game      *gam,
                        struct s_resources *res );

int game_menu_help( struct s_allegro   *alg,
                    struct s_config    *cfg,
                    struct s_game      *gam,
                    struct s_resources *res );

#endif // MENU_H_INCLUDED
