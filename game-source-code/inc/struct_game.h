#ifndef STRUCT_GAME_H_INCLUDED
#define STRUCT_GAME_H_INCLUDED

#include <stdint.h>

enum
  {
  GAME_STATE_MENU          = 0,
  GAME_STATE_MENU_SETTINGS = 1,
  GAME_STATE_MENU_HELP     = 2,
  GAME_STATE_PLAY          = 3,
  GAME_STATE_EDITOR        = 4,
  GAME_STATE_EXIT          = 5,

  GAME_TYPE_PLAY   = 0,
  GAME_TYPE_CUSTOM = 1,

  GAME_LEVEL_STATUS_COMPLETED = 0,
  GAME_LEVEL_STATUS_AVIABLE   = 1,
  GAME_LEVEL_STATUS_LOCKED    = 2,

  GAME_PAGE_COUNT      = 2,
  GAME_LEVELS_PER_PAGE = 10,
  GAME_LEVEL_COUNT     = GAME_PAGE_COUNT * GAME_LEVELS_PER_PAGE,
  };

struct s_game
  {
  int  state;
  int  game_type;
  int  page;
  int  selected_level;
  char level_path[ 1024 ];

  struct s_level
    {
    int8_t status;
    } level[ GAME_LEVEL_COUNT ];
  };

struct s_game *gam_create_game_struct();
void           gam_destroy_game_struct( struct s_game *gam );

void gam_keep_three_levels_aviable( struct s_game *gam );

void gam_select_next_level( struct s_game *gam );

// Progress:
void gam_reset_progress( struct s_game *gam );
int  gam_save_progress ( struct s_game *gam, const char *file_path );
int  gam_load_progress ( struct s_game *gam, const char *file_path );

void gam_page_plus ( struct s_game *gam );
void gam_page_minus( struct s_game *gam );

void gam_set_game_type           ( struct s_game *gam, int new_game_type );
void gam_set_state               ( struct s_game *gam, int new_state );
void gam_set_level_path          ( struct s_game *gam, const char *path );
void gam_set_selected_level      ( struct s_game *gam, int new_selected_level );
void gam_set_current_level_status( struct s_game *gam, int new_status );

int         gam_get_page                 ( struct s_game *gam );
int         gam_get_game_type            ( struct s_game *gam );
int         gam_get_selected_level       ( struct s_game *gam );
int         gam_get_state                ( struct s_game *gam );
int         gam_get_selected_level_status( struct s_game *gam );
const char *gam_get_level_file_path      ( struct s_game *gam );

#endif // STRUCT_GAME_H_INCLUDED
