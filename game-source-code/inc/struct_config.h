#ifndef STRUCT_CONFIG_H_INCLUDED
#define STRUCT_CONFIG_H_INCLUDED

#include <stdint.h>

enum
  {
  CFG_SCALE_1 = 1,
  CFG_SCALE_2 = 2,
  CFG_SCALE_3 = 3,

  CFG_SCALE_MIN = 1,
  CFG_SCALE_MAX = 3,
  };

struct s_config
  {
  int8_t scale;
  };

struct s_config *cfg_create_struct ();
void             cfg_destroy_struct( struct s_config *cfg );

void cfg_set_default_values( struct s_config *cfg );

int cfg_load_from_file( struct s_config *cfg, const char *file_path );
int cfg_save_to_file  ( struct s_config *cfg, const char *file_path );


int cfg_get_scale      ( struct s_config *cfg );

void cfg_set_scale      ( struct s_config *cfg, int new_scale );

#endif // STRUCT_CONFIG_H_INCLUDED
