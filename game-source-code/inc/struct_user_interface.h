#ifndef STRUCT_USER_INTERFACE_H_INCLUDED
#define STRUCT_USER_INTERFACE_H_INCLUDED

enum
  {
  UI_TYPE_BUTTON = 0,
  UI_TYPE_IMAGE  = 1,

  UI_VISIBLE_NO  = 0,
  UI_VISIBLE_YES = 1,

  UIE_NONE = -1,

  // Menu elements:
  UIE_M_BUTTON_LEVEL_0 = 0,
  UIE_M_BUTTON_LEVEL_1 = 1,
  UIE_M_BUTTON_LEVEL_2 = 2,
  UIE_M_BUTTON_LEVEL_3 = 3,
  UIE_M_BUTTON_LEVEL_4 = 4,
  UIE_M_BUTTON_LEVEL_5 = 5,
  UIE_M_BUTTON_LEVEL_6 = 6,
  UIE_M_BUTTON_LEVEL_7 = 7,
  UIE_M_BUTTON_LEVEL_8 = 8,
  UIE_M_BUTTON_LEVEL_9 = 9,

  UIE_M_BUTTON_PLAY    = 11,
  UIE_M_BUTTON_CONFIG  = 12,
  UIE_M_BUTTON_EXIT    = 13,
  UIE_M_BUTTON_EDITOR  = 14,
  UIE_M_BUTTON_CUSTOM  = 15,
  UIE_M_BUTTON_HELP    = 16,
  UIE_M_BUTTON_PAGE_MINUS = 17,
  UIE_M_BUTTON_PAGE_PLUS  = 18,

  // Menu settings:
  UIE_MS_BUTTON_SCALE_LEFT     = 1,
  UIE_MS_BUTTON_SCALE_RIGHT    = 2,
  UIE_MS_BUTTON_APPLY          = 3,
  UIE_MS_BUTTON_MENU           = 4,
  UIE_MS_BUTTON_RESET_PROGRESS = 5,

  // Menu help:
  UIE_MH_BUTTON_MENU       = 1,
  UIE_MH_BUTTON_PAGE_MINUS = 2,
  UIE_MH_BUTTON_PAGE_PLUS  = 3,

  // Editor elements:
  UIE_E_BUTTON_SHIFT_UP          = 1,
  UIE_E_BUTTON_SHIFT_RIGHT       = 2,
  UIE_E_BUTTON_SHIFT_DOWN        = 3,
  UIE_E_BUTTON_SHIFT_LEFT        = 4,
  UIE_E_BUTTON_CLEAR             = 5,
  UIE_E_BUTTON_LOAD              = 6,
  UIE_E_BUTTON_SAVE              = 7,
  UIE_E_BUTTON_MENU              = 8,
  UIE_E_BUTTON_BRUSH_WALL        = 9,
  UIE_E_BUTTON_BRUSH_DESTINATION = 10,
  UIE_E_BUTTON_BRUSH_CRATE       = 11,
  UIE_E_BUTTON_BRUSH_START       = 12,

  // Play elements:
  UIE_P_BUTTON_MENU  = 1,
  UIE_P_BUTTON_RESET = 2,
  UIE_P_BUTTON_UNDO  = 3,

  // Play popup window:
  UIE_P_PW_BUTTON_MENU       = 4,
  UIE_P_PW_BUTTON_NEXT_LEVEL = 5,

  UI_ELEMENT_MAX_COUNT = 32
  };

struct s_user_interface
  {
  int element_count;

  struct s_element
    {
    int dx;
    int dy;
    int cx;
    int cy;
    int cw;
    int ch;

    int type;
    int visible;

    int eid;
    int sid;
    } element[ UI_ELEMENT_MAX_COUNT ];
  };

struct s_user_interface *uin_create_struct ();
void                     uin_destroy_struct( struct s_user_interface *uin );

void uin_clear( struct s_user_interface *uin );

int uin_add_element( struct s_user_interface *uin,
                     int dx,
                     int dy,
                     int cx,
                     int cy,
                     int cw,
                     int ch,
                     int type,
                     int visible,
                     int eid,
                     int sid );

int uin_check_mouse_click( struct s_user_interface *uin, int mx, int my );

// Test ui set build functions:
void uin_build_menu         ( struct s_user_interface *uin );
void uin_build_menu_settings( struct s_user_interface *uin );
void uin_build_menu_help    ( struct s_user_interface *uin );
void uin_build_editor_ui    ( struct s_user_interface *uin );
void uin_build_play_ui      ( struct s_user_interface *uin );

void uin_build_pw_play      ( struct s_user_interface *uin );
void uin_build_pw_custom    ( struct s_user_interface *uin );

#endif // STRUCT_USER_INTERFACE_H_INCLUDED
