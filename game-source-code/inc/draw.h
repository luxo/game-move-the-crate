#ifndef DRAW_H_INCLUDED
#define DRAW_H_INCLUDED

#include "stdbool.h"

struct s_config;
struct s_game;
struct s_map;
struct s_player;
struct s_resources;
struct s_user_interface;

void draw_player( struct s_player *plr, struct s_resources *res );

void draw_map( struct s_map *map,
               struct s_resources *res,
               bool draw_crates,                  // Crates
               bool draw_destinations,            // Destinations
               bool draw_player_start_position ); // Player start position

void draw_marker        ( struct s_resources *res, int x, int y );
void draw_brush_selector( struct s_resources *res, int brush );
void draw_c_d_counts    ( struct s_map *map, struct s_resources *res );

void draw_menu_level_selection( struct s_game *gam, struct s_resources *res );

// Menu settings:
void draw_menu_settings( struct s_config    *cfg,
                         struct s_game      *gam,
                         struct s_resources *res,
                         int scale );

void draw_menu_help( struct s_resources *res, int page );

void draw_ui( struct s_resources *res, struct s_user_interface *uin );


void draw_popup_window( struct s_resources *res );

void draw_level_number_text( struct s_game *gam, struct s_resources *res );

#endif // DRAW_H_INCLUDED
