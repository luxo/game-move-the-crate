#ifndef STRUCT_RESOURCES_H_INCLUDED
#define STRUCT_RESOURCES_H_INCLUDED

#include <stdbool.h>

#include "allegro5/allegro.h"
#include "allegro5/allegro_font.h"

#include "sprites.h"

enum
  {
  // Sprite sheet:
  RES_SPRITE_SHEET_WIDTH  = 512,
  RES_SPRITE_SHEET_HEIGHT = 512,

  // Sprites:
  // Size:
  RES_SPRITE_WIDTH  = 32,
  RES_SPRITE_HEIGHT = 32,
  };

struct s_resources
  {
  ALLEGRO_FONT   *font;
  ALLEGRO_BITMAP *sprite_sheet;
  ALLEGRO_BITMAP *sprite[ SPR_COUNT ];
  bool            initialized;
  };

struct s_resources *res_create_resources_struct();
void                res_destroy_resources_struct( struct s_resources *res );

void res_destroy_all_internal_data( struct s_resources *res );

int res_initialize     ( struct s_resources *res );
int res_load_resources ( struct s_resources *res );

ALLEGRO_BITMAP *res_get_sprite( struct s_resources *res, int sprite_id );

#endif // STRUCT_RESOURCES_H_INCLUDED
